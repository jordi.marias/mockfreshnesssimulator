import re

# Your input string containing the trip information
input_data = """
<trip id="veh0" type="veh_passenger" depart="0.00" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh1" type="veh_passenger" depart="2.61" departLane="best" departSpeed="max" from="-775343568#1" to="129648702#4"/>
    <trip id="veh2" type="veh_passenger" depart="5.23" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh3" type="veh_passenger" depart="7.84" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh4" type="veh_passenger" depart="10.45" departLane="best" departSpeed="max" from="-54856713" to="32036700#4"/>
    <trip id="veh5" type="veh_passenger" depart="13.06" departLane="best" departSpeed="max" from="-54856713" to="543400544#1"/>
    <trip id="veh6" type="veh_passenger" depart="15.68" departLane="best" departSpeed="max" from="54856708#0" to="122307615#0"/>
    <trip id="veh7" type="veh_passenger" depart="18.29" departLane="best" departSpeed="max" from="-87408929#4" to="116315042"/>
    <trip id="veh8" type="veh_passenger" depart="20.90" departLane="best" from="-543400544#0" to="116315042"/>
    <trip id="veh9" type="veh_passenger" depart="23.52" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh10" type="veh_passenger" depart="26.13" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh12" type="veh_passenger" depart="31.36" departLane="best" departSpeed="max" from="123214371#0" to="35549511#1"/>
    <trip id="veh13" type="veh_passenger" depart="33.97" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh14" type="veh_passenger" depart="36.58" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh15" type="veh_passenger" depart="39.19" departLane="best" departSpeed="max" from="449606871" to="-123214371#0"/>
    <trip id="veh16" type="veh_passenger" depart="41.81" departLane="best" departSpeed="max" from="-54856713" to="23247531#9"/>
    <trip id="veh18" type="veh_passenger" depart="47.03" departLane="best" departSpeed="max" from="-543400544#1" to="230423229"/>
    <trip id="veh19" type="veh_passenger" depart="49.65" departLane="best" departSpeed="max" from="449606867" to="262841306#2"/>
    <trip id="veh20" type="veh_passenger" depart="52.26" departLane="best" departSpeed="max" from="123214371#0" to="122307616"/>
    <trip id="veh21" type="veh_passenger" depart="54.87" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh23" type="veh_passenger" depart="60.10" departLane="best" from="830793057#1" to="543400544#1"/>
    <trip id="veh25" type="veh_passenger" depart="65.32" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh26" type="veh_passenger" depart="67.94" departLane="best" departSpeed="max" from="-87408929#4" to="230423229"/>
    <trip id="veh27" type="veh_passenger" depart="70.55" departLane="best" from="-543748715" to="775343568#1"/>
    <trip id="veh28" type="veh_passenger" depart="73.16" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh33" type="veh_passenger" depart="86.23" departLane="best" departSpeed="max" from="23492230#0" to="122307620#3"/>
    <trip id="veh34" type="veh_passenger" depart="88.84" departLane="best" from="910000958" to="32036700#4"/>
    <trip id="veh35" type="veh_passenger" depart="91.45" departLane="best" from="-396767222#1" to="-123214371#0"/>
    <trip id="veh36" type="veh_passenger" depart="94.07" departLane="best" from="-879196982#0" to="35549511#1"/>
    <trip id="veh37" type="veh_passenger" depart="96.68" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh39" type="veh_passenger" depart="101.91" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh41" type="veh_passenger" depart="107.13" departLane="best" departSpeed="max" from="-23492230#1" to="775343568#1"/>
    <trip id="veh42" type="veh_passenger" depart="109.74" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh44" type="veh_passenger" depart="114.97" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh45" type="veh_passenger" depart="117.58" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh46" type="veh_passenger" depart="120.20" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh47" type="veh_passenger" depart="122.81" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh48" type="veh_passenger" depart="125.42" departLane="best" from="-1162513994#0" to="23247531#9"/>
    <trip id="veh49" type="veh_passenger" depart="128.04" departLane="best" from="-122307620#3" to="775343568#1"/>
    <trip id="veh50" type="veh_passenger" depart="130.65" departLane="best" departSpeed="max" from="-775343568#1" to="32036701#1"/>
    <trip id="veh51" type="veh_passenger" depart="133.26" departLane="best" departSpeed="max" from="54856708#0" to="775343568#1"/>
    <trip id="veh53" type="veh_passenger" depart="138.49" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh55" type="veh_passenger" depart="143.71" departLane="best" departSpeed="max" from="-775343568#1" to="68506246#1"/>
    <trip id="veh56" type="veh_passenger" depart="146.33" departLane="best" departSpeed="max" from="23492230#0" to="-122307619#0"/>
    <trip id="veh57" type="veh_passenger" depart="148.94" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh58" type="veh_passenger" depart="151.55" departLane="best" from="877826826" to="543400544#1"/>
    <trip id="veh59" type="veh_passenger" depart="154.17" departLane="best" departSpeed="max" from="23492230#0" to="32036700#4"/>
    <trip id="veh61" type="veh_passenger" depart="159.39" departLane="best" departSpeed="max" from="-87408929#4" to="543400544#1"/>
    <trip id="veh62" type="veh_passenger" depart="162.00" departLane="best" departSpeed="max" from="-54856713" to="122307620#3"/>
    <trip id="veh63" type="veh_passenger" depart="164.62" departLane="best" departSpeed="max" from="-54856713" to="68506246#1"/>
    <trip id="veh64" type="veh_passenger" depart="167.23" departLane="best" departSpeed="max" from="54856708#0" to="-553278873#2"/>
    <trip id="veh65" type="veh_passenger" depart="169.84" departLane="best" departSpeed="max" from="-54856713" to="54856677#1"/>
    <trip id="veh66" type="veh_passenger" depart="172.46" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh67" type="veh_passenger" depart="175.07" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh69" type="veh_passenger" depart="180.30" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh70" type="veh_passenger" depart="182.91" departLane="best" from="775343565#0" to="116315042"/>
    <trip id="veh71" type="veh_passenger" depart="185.52" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh74" type="veh_passenger" depart="193.36" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh75" type="veh_passenger" depart="195.97" departLane="best" departSpeed="max" from="-775343568#1" to="775343565#1"/>
    <trip id="veh76" type="veh_passenger" depart="198.59" departLane="best" departSpeed="max" from="23492230#0" to="68506246#1"/>
    <trip id="veh78" type="veh_passenger" depart="203.81" departLane="best" from="916493566#4" to="54856713"/>
    <trip id="veh79" type="veh_passenger" depart="206.43" departLane="best" departSpeed="max" from="68506239#0" to="775343568#1"/>
    <trip id="veh80" type="veh_passenger" depart="209.04" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh82" type="veh_passenger" depart="214.26" departLane="best" departSpeed="max" from="123214371#0" to="87408929#4"/>
    <trip id="veh83" type="veh_passenger" depart="216.88" departLane="best" departSpeed="max" from="68506239#0" to="-830793056#0"/>
    <trip id="veh85" type="veh_passenger" depart="222.10" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh86" type="veh_passenger" depart="224.72" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh88" type="veh_passenger" depart="229.94" departLane="best" departSpeed="max" from="-543400544#1" to="-867328945#2"/>
    <trip id="veh89" type="veh_passenger" depart="232.55" departLane="best" departSpeed="max" from="-262841306#2" to="116315042"/>
    <trip id="veh90" type="veh_passenger" depart="235.17" departLane="best" from="320838381#0" to="54856677#1"/>
    <trip id="veh91" type="veh_passenger" depart="237.78" departLane="best" departSpeed="max" from="-54856713" to="116315042"/>
    <trip id="veh94" type="veh_passenger" depart="245.62" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh95" type="veh_passenger" depart="248.23" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh97" type="veh_passenger" depart="253.46" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh100" type="veh_passenger" depart="261.30" departLane="best" from="916493566#0" to="879196982#1"/>
    <trip id="veh101" type="veh_passenger" depart="263.91" departLane="best" departSpeed="max" from="-262841306#2" to="-122307619#1"/>
    <trip id="veh102" type="veh_passenger" depart="266.52" departLane="best" from="549755456#3" to="68506246#1"/>
    <trip id="veh103" type="veh_passenger" depart="269.14" departLane="best" departSpeed="max" from="123214371#0" to="116315042"/>
    <trip id="veh104" type="veh_passenger" depart="271.75" departLane="best" departSpeed="max" from="449606867" to="87408929#4"/>
    <trip id="veh105" type="veh_passenger" depart="274.36" departLane="best" departSpeed="max" from="449606867" to="-122307616"/>
    <trip id="veh106" type="veh_passenger" depart="276.98" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh107" type="veh_passenger" depart="279.59" departLane="best" departSpeed="max" from="449606867" to="775343568#1"/>
    <trip id="veh108" type="veh_passenger" depart="282.20" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh109" type="veh_passenger" depart="284.81" departLane="best" departSpeed="max" from="-543400544#1" to="35549511#1"/>
    <trip id="veh110" type="veh_passenger" depart="287.43" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh111" type="veh_passenger" depart="290.04" departLane="best" departSpeed="max" from="449606867" to="230423229"/>
    <trip id="veh113" type="veh_passenger" depart="295.27" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh114" type="veh_passenger" depart="297.88" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh116" type="veh_passenger" depart="303.11" departLane="best" departSpeed="max" from="449606867" to="230423229"/>
    <trip id="veh117" type="veh_passenger" depart="305.72" departLane="best" departSpeed="max" from="23492230#0" to="122307631#0"/>
    <trip id="veh118" type="veh_passenger" depart="308.33" departLane="best" departSpeed="max" from="68506239#0" to="35549511#1"/>
    <trip id="veh119" type="veh_passenger" depart="310.94" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh120" type="veh_passenger" depart="313.56" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh121" type="veh_passenger" depart="316.17" departLane="best" departSpeed="max" from="-775343568#1" to="68506246#1"/>
    <trip id="veh122" type="veh_passenger" depart="318.78" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh124" type="veh_passenger" depart="324.01" departLane="best" from="-775343568#0" to="116315042"/>
    <trip id="veh125" type="veh_passenger" depart="326.62" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh126" type="veh_passenger" depart="329.23" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh127" type="veh_passenger" depart="331.85" departLane="best" departSpeed="max" from="-543400544#1" to="775343568#1"/>
    <trip id="veh128" type="veh_passenger" depart="334.46" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh129" type="veh_passenger" depart="337.07" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh131" type="veh_passenger" depart="342.30" departLane="best" departSpeed="max" from="-87408929#4" to="775343568#1"/>
    <trip id="veh132" type="veh_passenger" depart="344.91" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh133" type="veh_passenger" depart="347.53" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh134" type="veh_passenger" depart="350.14" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh135" type="veh_passenger" depart="352.75" departLane="best" departSpeed="max" from="54856708#0" to="68506239#9"/>
    <trip id="veh136" type="veh_passenger" depart="355.36" departLane="best" from="237522645#0" to="23247531#9"/>
    <trip id="veh138" type="veh_passenger" depart="360.59" departLane="best" departSpeed="max" from="-87408929#4" to="68506246#1"/>
    <trip id="veh139" type="veh_passenger" depart="363.20" departLane="best" departSpeed="max" from="68506239#0" to="230423229"/>
    <trip id="veh141" type="veh_passenger" depart="368.43" departLane="best" from="-68506264" to="35549511#1"/>
    <trip id="veh142" type="veh_passenger" depart="371.04" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh143" type="veh_passenger" depart="373.66" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh144" type="veh_passenger" depart="376.27" departLane="best" departSpeed="max" from="68506239#0" to="230423229"/>
    <trip id="veh146" type="veh_passenger" depart="381.49" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh148" type="veh_passenger" depart="386.72" departLane="best" departSpeed="max" from="123214371#0" to="543400544#1"/>
    <trip id="veh149" type="veh_passenger" depart="389.33" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh150" type="veh_passenger" depart="391.95" departLane="best" from="-116315037#1" to="775343568#1"/>
    <trip id="veh152" type="veh_passenger" depart="397.17" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh153" type="veh_passenger" depart="399.79" departLane="best" departSpeed="max" from="123214371#0" to="-553278873#0"/>
    <trip id="veh154" type="veh_passenger" depart="402.40" departLane="best" departSpeed="max" from="449606867" to="23247531#9"/>
    <trip id="veh155" type="veh_passenger" depart="405.01" departLane="best" departSpeed="max" from="-87408929#4" to="551933607#1"/>
    <trip id="veh156" type="veh_passenger" depart="407.62" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh158" type="veh_passenger" depart="412.85" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh160" type="veh_passenger" depart="418.08" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh161" type="veh_passenger" depart="420.69" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh162" type="veh_passenger" depart="423.30" departLane="best" departSpeed="max" from="-775343568#1" to="35549511#1"/>
    <trip id="veh163" type="veh_passenger" depart="425.91" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh165" type="veh_passenger" depart="431.14" departLane="best" departSpeed="max" from="23492230#0" to="122307620#0"/>
    <trip id="veh167" type="veh_passenger" depart="436.37" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh168" type="veh_passenger" depart="438.98" departLane="best" from="-32023368#1" to="32036700#4"/>
    <trip id="veh169" type="veh_passenger" depart="441.59" departLane="best" departSpeed="max" from="449606867" to="23247531#9"/>
    <trip id="veh170" type="veh_passenger" depart="444.21" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh171" type="veh_passenger" depart="446.82" departLane="best" departSpeed="max" from="-543400544#1" to="-123214371#0"/>
    <trip id="veh173" type="veh_passenger" depart="452.04" departLane="best" departSpeed="max" from="54856708#0" to="68506264"/>
    <trip id="veh174" type="veh_passenger" depart="454.66" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh175" type="veh_passenger" depart="457.27" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh177" type="veh_passenger" depart="462.50" departLane="best" departSpeed="max" from="449606867" to="-122307615#1"/>
    <trip id="veh180" type="veh_passenger" depart="470.34" departLane="best" departSpeed="max" from="-543400544#1" to="830793057#0"/>
    <trip id="veh182" type="veh_passenger" depart="475.56" departLane="best" from="-775343568#0" to="551933608#2"/>
    <trip id="veh184" type="veh_passenger" depart="480.79" departLane="best" departSpeed="max" from="449606871" to="87408929#4"/>
    <trip id="veh185" type="veh_passenger" depart="483.40" departLane="best" from="320838381#1" to="775343568#1"/>
    <trip id="veh186" type="veh_passenger" depart="486.01" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh187" type="veh_passenger" depart="488.63" departLane="best" departSpeed="max" from="449606871" to="32036700#4"/>
    <trip id="veh188" type="veh_passenger" depart="491.24" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh190" type="veh_passenger" depart="496.47" departLane="best" departSpeed="max" from="54856708#0" to="32036704#2"/>
    <trip id="veh191" type="veh_passenger" depart="499.08" departLane="best" departSpeed="max" from="449606867" to="230423229"/>
    <trip id="veh192" type="veh_passenger" depart="501.69" departLane="best" departSpeed="max" from="-262841306#2" to="230423229"/>
    <trip id="veh193" type="veh_passenger" depart="504.30" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#9"/>
    <trip id="veh195" type="veh_passenger" depart="509.53" departLane="best" departSpeed="max" from="-54856713" to="54856677#1"/>
    <trip id="veh196" type="veh_passenger" depart="512.14" departLane="best" departSpeed="max" from="449606871" to="543400544#0"/>
    <trip id="veh197" type="veh_passenger" depart="514.76" departLane="best" departSpeed="max" from="23492230#0" to="35549511#1"/>
    <trip id="veh198" type="veh_passenger" depart="517.37" departLane="best" departSpeed="max" from="23492230#0" to="68506246#1"/>
    <trip id="veh200" type="veh_passenger" depart="522.59" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh201" type="veh_passenger" depart="525.21" departLane="best" departSpeed="max" from="123214371#0" to="122307615#0"/>
    <trip id="veh203" type="veh_passenger" depart="530.43" departLane="best" departSpeed="max" from="54856708#0" to="775343568#1"/>
    <trip id="veh206" type="veh_passenger" depart="538.27" departLane="best" departSpeed="max" from="-54856713" to="122307620#3"/>
    <trip id="veh207" type="veh_passenger" depart="540.89" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh208" type="veh_passenger" depart="543.50" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh209" type="veh_passenger" depart="546.11" departLane="best" from="26293951#6" to="116315042"/>
    <trip id="veh210" type="veh_passenger" depart="548.72" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh211" type="veh_passenger" depart="551.34" departLane="best" from="-179721528" to="396767223#1"/>
    <trip id="veh212" type="veh_passenger" depart="553.95" departLane="best" departSpeed="max" from="-170958183#1" to="543400544#1"/>
    <trip id="veh214" type="veh_passenger" depart="559.18" departLane="best" from="867340083" to="262841306#2"/>
    <trip id="veh215" type="veh_passenger" depart="561.79" departLane="best" from="-543748715" to="23247531#9"/>
    <trip id="veh216" type="veh_passenger" depart="564.40" departLane="best" departSpeed="max" from="54856708#0" to="262841306#2"/>
    <trip id="veh217" type="veh_passenger" depart="567.02" departLane="best" departSpeed="max" from="449606871" to="54856713"/>
    <trip id="veh218" type="veh_passenger" depart="569.63" departLane="best" departSpeed="max" from="-775343568#1" to="1162513994#1"/>
    <trip id="veh219" type="veh_passenger" depart="572.24" departLane="best" from="916493566#5" to="23247531#9"/>
    <trip id="veh222" type="veh_passenger" depart="580.08" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh223" type="veh_passenger" depart="582.69" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh226" type="veh_passenger" depart="590.53" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh227" type="veh_passenger" depart="593.15" departLane="best" departSpeed="max" from="-543400544#1" to="-123214371#0"/>
    <trip id="veh229" type="veh_passenger" depart="598.37" departLane="best" departSpeed="max" from="449606871" to="-830793057#0"/>
    <trip id="veh230" type="veh_passenger" depart="600.98" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh232" type="veh_passenger" depart="606.21" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh233" type="veh_passenger" depart="608.82" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh234" type="veh_passenger" depart="611.44" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh235" type="veh_passenger" depart="614.05" departLane="best" departSpeed="max" from="23492230#0" to="262841306#2"/>
    <trip id="veh237" type="veh_passenger" depart="619.28" departLane="best" departSpeed="max" from="54856708#0" to="54856713"/>
    <trip id="veh238" type="veh_passenger" depart="621.89" departLane="best" departSpeed="max" from="-543400544#1" to="-867328945#2"/>
    <trip id="veh239" type="veh_passenger" depart="624.50" departLane="best" departSpeed="max" from="449606867" to="54856713"/>
    <trip id="veh241" type="veh_passenger" depart="629.73" departLane="best" departSpeed="max" from="449606871" to="87408929#4"/>
    <trip id="veh242" type="veh_passenger" depart="632.34" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh244" type="veh_passenger" depart="637.57" departLane="best" departSpeed="max" from="-775343568#1" to="571426636#2"/>
    <trip id="veh245" type="veh_passenger" depart="640.18" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh247" type="veh_passenger" depart="645.40" departLane="best" from="26293951#3" to="543400544#1"/>
    <trip id="veh248" type="veh_passenger" depart="648.02" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh250" type="veh_passenger" depart="653.24" departLane="best" departSpeed="max" from="-87408929#4" to="68506246#1"/>
    <trip id="veh251" type="veh_passenger" depart="655.86" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh253" type="veh_passenger" depart="661.08" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh254" type="veh_passenger" depart="663.70" departLane="best" departSpeed="max" from="-543400544#1" to="-122307617"/>
    <trip id="veh255" type="veh_passenger" depart="666.31" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh256" type="veh_passenger" depart="668.92" departLane="best" departSpeed="max" from="32024073" to="30014919"/>
    <trip id="veh257" type="veh_passenger" depart="671.53" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh258" type="veh_passenger" depart="674.15" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh259" type="veh_passenger" depart="676.76" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh260" type="veh_passenger" depart="679.37" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh261" type="veh_passenger" depart="681.99" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh262" type="veh_passenger" depart="684.60" departLane="best" departSpeed="max" from="-543400544#1" to="122307620#1"/>
    <trip id="veh263" type="veh_passenger" depart="687.21" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh265" type="veh_passenger" depart="692.44" departLane="best" departSpeed="max" from="-543400544#1" to="-122307619#1"/>
    <trip id="veh266" type="veh_passenger" depart="695.05" departLane="best" departSpeed="max" from="68506239#0" to="35549511#1"/>
    <trip id="veh267" type="veh_passenger" depart="697.66" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh268" type="veh_passenger" depart="700.28" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh270" type="veh_passenger" depart="705.50" departLane="best" departSpeed="max" from="-543400544#1" to="179721528"/>
    <trip id="veh272" type="veh_passenger" depart="710.73" departLane="best" from="-262841306#1" to="116315042"/>
    <trip id="veh274" type="veh_passenger" depart="715.96" departLane="best" from="871687213#4" to="116315042"/>
    <trip id="veh275" type="veh_passenger" depart="718.57" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh277" type="veh_passenger" depart="723.79" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh278" type="veh_passenger" depart="726.41" departLane="best" departSpeed="max" from="23492230#0" to="68506246#1"/>
    <trip id="veh279" type="veh_passenger" depart="729.02" departLane="best" from="830793057#1" to="23247531#9"/>
    <trip id="veh280" type="veh_passenger" depart="731.63" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh281" type="veh_passenger" depart="734.25" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh282" type="veh_passenger" depart="736.86" departLane="best" departSpeed="max" from="-543400544#1" to="-122307619#0"/>
    <trip id="veh283" type="veh_passenger" depart="739.47" departLane="best" departSpeed="max" from="-543400544#1" to="-830793056#0"/>
    <trip id="veh285" type="veh_passenger" depart="744.70" departLane="best" from="32036704#0" to="23247531#9"/>
    <trip id="veh286" type="veh_passenger" depart="747.31" departLane="best" departSpeed="max" from="-54856713" to="54856677#1"/>
    <trip id="veh288" type="veh_passenger" depart="752.54" departLane="best" departSpeed="max" from="449606871" to="775343568#0"/>
    <trip id="veh289" type="veh_passenger" depart="755.15" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh290" type="veh_passenger" depart="757.76" departLane="best" departSpeed="max" from="449606867" to="775343568#1"/>
    <trip id="veh291" type="veh_passenger" depart="760.38" departLane="best" departSpeed="max" from="23492230#0" to="35549511#1"/>
    <trip id="veh292" type="veh_passenger" depart="762.99" departLane="best" departSpeed="max" from="449606867" to="68506246#1"/>
    <trip id="veh293" type="veh_passenger" depart="765.60" departLane="best" departSpeed="max" from="54856708#0" to="-553278873#3"/>
    <trip id="veh294" type="veh_passenger" depart="768.21" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh295" type="veh_passenger" depart="770.83" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh296" type="veh_passenger" depart="773.44" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh297" type="veh_passenger" depart="776.05" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh298" type="veh_passenger" depart="778.67" departLane="best" departSpeed="max" from="68506239#0" to="32036700#4"/>
    <trip id="veh302" type="veh_passenger" depart="789.12" departLane="best" from="-396767222#1" to="-184444814#0"/>
    <trip id="veh303" type="veh_passenger" depart="791.73" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh304" type="veh_passenger" depart="794.34" departLane="best" departSpeed="max" from="68506239#0" to="35814811#3"/>
    <trip id="veh305" type="veh_passenger" depart="796.96" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh308" type="veh_passenger" depart="804.80" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh311" type="veh_passenger" depart="812.64" departLane="best" from="116801392#4" to="23247531#9"/>
    <trip id="veh312" type="veh_passenger" depart="815.25" departLane="best" departSpeed="max" from="449606871" to="68506239#9"/>
    <trip id="veh313" type="veh_passenger" depart="817.86" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh314" type="veh_passenger" depart="820.47" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh315" type="veh_passenger" depart="823.09" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh316" type="veh_passenger" depart="825.70" departLane="best" departSpeed="max" from="23492230#0" to="32036700#4"/>
    <trip id="veh317" type="veh_passenger" depart="828.31" departLane="best" from="830793055" to="543400544#1"/>
    <trip id="veh318" type="veh_passenger" depart="830.93" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh319" type="veh_passenger" depart="833.54" departLane="best" from="538552772#1" to="543400544#1"/>
    <trip id="veh320" type="veh_passenger" depart="836.15" departLane="best" from="54856674#1" to="116315042"/>
    <trip id="veh321" type="veh_passenger" depart="838.76" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh322" type="veh_passenger" depart="841.38" departLane="best" departSpeed="max" from="-543400544#1" to="35549511#1"/>
    <trip id="veh324" type="veh_passenger" depart="846.60" departLane="best" from="867340083" to="68506246#1"/>
    <trip id="veh325" type="veh_passenger" depart="849.22" departLane="best" from="237914517#1" to="116315042"/>
    <trip id="veh326" type="veh_passenger" depart="851.83" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh327" type="veh_passenger" depart="854.44" departLane="best" departSpeed="max" from="449606871" to="32036700#4"/>
    <trip id="veh328" type="veh_passenger" depart="857.06" departLane="best" departSpeed="max" from="-775343568#1" to="54856713"/>
    <trip id="veh331" type="veh_passenger" depart="864.89" departLane="best" departSpeed="max" from="68506239#0" to="32036700#4"/>
    <trip id="veh332" type="veh_passenger" depart="867.51" departLane="best" from="54856715#0" to="68506246#1"/>
    <trip id="veh333" type="veh_passenger" depart="870.12" departLane="best" departSpeed="max" from="-23492230#1" to="775343568#1"/>
    <trip id="veh335" type="veh_passenger" depart="875.35" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#9"/>
    <trip id="veh340" type="veh_passenger" depart="888.41" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh341" type="veh_passenger" depart="891.02" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh343" type="veh_passenger" depart="896.25" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh344" type="veh_passenger" depart="898.86" departLane="best" departSpeed="max" from="449606867" to="54856713"/>
    <trip id="veh345" type="veh_passenger" depart="901.48" departLane="best" from="68506239#9" to="32036700#4"/>
    <trip id="veh346" type="veh_passenger" depart="904.09" departLane="best" departSpeed="max" from="-170958183#1" to="32036700#4"/>
    <trip id="veh348" type="veh_passenger" depart="909.32" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh351" type="veh_passenger" depart="917.15" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh352" type="veh_passenger" depart="919.77" departLane="best" departSpeed="max" from="449606867" to="-571426636#2"/>
    <trip id="veh353" type="veh_passenger" depart="922.38" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh354" type="veh_passenger" depart="924.99" departLane="best" departSpeed="max" from="123214371#0" to="23247531#9"/>
    <trip id="veh356" type="veh_passenger" depart="930.22" departLane="best" departSpeed="max" from="68506239#0" to="-543754801"/>
    <trip id="veh358" type="veh_passenger" depart="935.45" departLane="best" departSpeed="max" from="449606871" to="68506246#1"/>
    <trip id="veh360" type="veh_passenger" depart="940.67" departLane="best" departSpeed="max" from="-775343568#1" to="262841306#2"/>
    <trip id="veh361" type="veh_passenger" depart="943.28" departLane="best" departSpeed="max" from="-543400544#1" to="35549511#1"/>
    <trip id="veh362" type="veh_passenger" depart="945.90" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#1"/>
    <trip id="veh363" type="veh_passenger" depart="948.51" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh364" type="veh_passenger" depart="951.12" departLane="best" departSpeed="max" from="-54856713" to="68506246#1"/>
    <trip id="veh366" type="veh_passenger" depart="956.35" departLane="best" departSpeed="max" from="-775343568#1" to="184444814#1"/>
    <trip id="veh368" type="veh_passenger" depart="961.57" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh370" type="veh_passenger" depart="966.80" departLane="best" from="116315037#1" to="543400544#1"/>
    <trip id="veh372" type="veh_passenger" depart="972.03" departLane="best" departSpeed="max" from="-775343568#1" to="54856715#0"/>
    <trip id="veh375" type="veh_passenger" depart="979.87" departLane="best" from="867340083" to="23247531#9"/>
    <trip id="veh376" type="veh_passenger" depart="982.48" departLane="best" from="68506239#4" to="543400544#1"/>
    <trip id="veh377" type="veh_passenger" depart="985.09" departLane="best" departSpeed="max" from="-775343568#1" to="538552772#0"/>
    <trip id="veh379" type="veh_passenger" depart="990.32" departLane="best" departSpeed="max" from="-170958183#1" to="543400544#1"/>
    <trip id="veh382" type="veh_passenger" depart="998.16" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh383" type="veh_passenger" depart="1000.77" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh384" type="veh_passenger" depart="1003.38" departLane="best" departSpeed="max" from="449606867" to="54856713"/>
    <trip id="veh385" type="veh_passenger" depart="1006.00" departLane="best" departSpeed="max" from="449606867" to="68506246#1"/>
    <trip id="veh386" type="veh_passenger" depart="1008.61" departLane="best" departSpeed="max" from="449606867" to="23247531#9"/>
    <trip id="veh387" type="veh_passenger" depart="1011.22" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh388" type="veh_passenger" depart="1013.83" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh391" type="veh_passenger" depart="1021.67" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh392" type="veh_passenger" depart="1024.29" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh393" type="veh_passenger" depart="1026.90" departLane="best" departSpeed="max" from="-87408929#4" to="116315042"/>
    <trip id="veh394" type="veh_passenger" depart="1029.51" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh395" type="veh_passenger" depart="1032.13" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh396" type="veh_passenger" depart="1034.74" departLane="best" departSpeed="max" from="449606871" to="87408929#4"/>
    <trip id="veh399" type="veh_passenger" depart="1042.58" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh400" type="veh_passenger" depart="1045.19" departLane="best" departSpeed="max" from="54856708#0" to="775343568#1"/>
    <trip id="veh402" type="veh_passenger" depart="1050.42" departLane="best" departSpeed="max" from="-170958183#1" to="68506246#1"/>
    <trip id="veh403" type="veh_passenger" depart="1053.03" departLane="best" from="31823239#2" to="68506246#1"/>
    <trip id="veh404" type="veh_passenger" depart="1055.64" departLane="best" departSpeed="max" from="-54856713" to="-396767222#1"/>
    <trip id="veh405" type="veh_passenger" depart="1058.25" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh407" type="veh_passenger" depart="1063.48" departLane="best" departSpeed="max" from="-543400544#1" to="775343568#1"/>
    <trip id="veh411" type="veh_passenger" depart="1073.93" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh412" type="veh_passenger" depart="1076.55" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh413" type="veh_passenger" depart="1079.16" departLane="best" departSpeed="max" from="68506239#0" to="-123214371#0"/>
    <trip id="veh414" type="veh_passenger" depart="1081.77" departLane="best" departSpeed="max" from="68506239#0" to="68506239#4"/>
    <trip id="veh415" type="veh_passenger" depart="1084.38" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh418" type="veh_passenger" depart="1092.22" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh419" type="veh_passenger" depart="1094.84" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh420" type="veh_passenger" depart="1097.45" departLane="best" from="553278873#3" to="-123214371#0"/>
    <trip id="veh423" type="veh_passenger" depart="1105.29" departLane="best" departSpeed="max" from="-543400544#1" to="122307620#1"/>
    <trip id="veh424" type="veh_passenger" depart="1107.90" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh425" type="veh_passenger" depart="1110.51" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh426" type="veh_passenger" depart="1113.13" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh427" type="veh_passenger" depart="1115.74" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh429" type="veh_passenger" depart="1120.97" departLane="best" departSpeed="max" from="-54856713" to="23247531#9"/>
    <trip id="veh430" type="veh_passenger" depart="1123.58" departLane="best" departSpeed="max" from="-543400544#1" to="-184444814#1"/>
    <trip id="veh432" type="veh_passenger" depart="1128.81" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh433" type="veh_passenger" depart="1131.42" departLane="best" departSpeed="max" from="-543400544#1" to="32036700#4"/>
    <trip id="veh434" type="veh_passenger" depart="1134.03" departLane="best" departSpeed="max" from="68506239#0" to="-123214371#0"/>
    <trip id="veh435" type="veh_passenger" depart="1136.64" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh436" type="veh_passenger" depart="1139.26" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh437" type="veh_passenger" depart="1141.87" departLane="best" departSpeed="max" from="68506239#0" to="396767222#0"/>
    <trip id="veh438" type="veh_passenger" depart="1144.48" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh439" type="veh_passenger" depart="1147.10" departLane="best" departSpeed="max" from="449606871" to="32036700#4"/>
    <trip id="veh440" type="veh_passenger" depart="1149.71" departLane="best" departSpeed="max" from="68506239#0" to="68506264"/>
    <trip id="veh441" type="veh_passenger" depart="1152.32" departLane="best" departSpeed="max" from="-262841306#2" to="230423229"/>
    <trip id="veh442" type="veh_passenger" depart="1154.93" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh443" type="veh_passenger" depart="1157.55" departLane="best" from="-396767222#1" to="116315042"/>
    <trip id="veh444" type="veh_passenger" depart="1160.16" departLane="best" departSpeed="max" from="449606871" to="68506239#10"/>
    <trip id="veh446" type="veh_passenger" depart="1165.39" departLane="best" departSpeed="max" from="449606871" to="-122307620#0"/>
    <trip id="veh449" type="veh_passenger" depart="1173.23" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh450" type="veh_passenger" depart="1175.84" departLane="best" departSpeed="max" from="123214371#0" to="116315042"/>
    <trip id="veh451" type="veh_passenger" depart="1178.45" departLane="best" departSpeed="max" from="68506239#0" to="543400545"/>
    <trip id="veh452" type="veh_passenger" depart="1181.06" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh454" type="veh_passenger" depart="1186.29" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh456" type="veh_passenger" depart="1191.52" departLane="best" departSpeed="max" from="-87408929#4" to="116315042"/>
    <trip id="veh457" type="veh_passenger" depart="1194.13" departLane="best" from="122307619#1" to="54856677#1"/>
    <trip id="veh458" type="veh_passenger" depart="1196.74" departLane="best" departSpeed="max" from="449606871" to="68506246#1"/>
    <trip id="veh459" type="veh_passenger" depart="1199.36" departLane="best" departSpeed="max" from="449606871" to="87408929#4"/>
    <trip id="veh460" type="veh_passenger" depart="1201.97" departLane="best" from="35814811#0" to="-887909226#0"/>
    <trip id="veh462" type="veh_passenger" depart="1207.19" departLane="best" departSpeed="max" from="-543400544#1" to="54856715#0"/>
    <trip id="veh464" type="veh_passenger" depart="1212.42" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh465" type="veh_passenger" depart="1215.03" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh466" type="veh_passenger" depart="1217.65" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh467" type="veh_passenger" depart="1220.26" departLane="best" departSpeed="max" from="68506239#0" to="54856713"/>
    <trip id="veh468" type="veh_passenger" depart="1222.87" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh469" type="veh_passenger" depart="1225.49" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh470" type="veh_passenger" depart="1228.10" departLane="best" departSpeed="max" from="-87408929#4" to="-122307615#1"/>
    <trip id="veh471" type="veh_passenger" depart="1230.71" departLane="best" departSpeed="max" from="54856708#0" to="538552772#1"/>
    <trip id="veh472" type="veh_passenger" depart="1233.32" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh473" type="veh_passenger" depart="1235.94" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh474" type="veh_passenger" depart="1238.55" departLane="best" departSpeed="max" from="449606871" to="-122307620#0"/>
    <trip id="veh475" type="veh_passenger" depart="1241.16" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh476" type="veh_passenger" depart="1243.78" departLane="best" departSpeed="max" from="54856708#0" to="543400544#0"/>
    <trip id="veh477" type="veh_passenger" depart="1246.39" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh478" type="veh_passenger" depart="1249.00" departLane="best" departSpeed="max" from="449606867" to="-314417325#2"/>
    <trip id="veh479" type="veh_passenger" depart="1251.61" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh480" type="veh_passenger" depart="1254.23" departLane="best" from="538552772#0" to="116315042"/>
    <trip id="veh481" type="veh_passenger" depart="1256.84" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh482" type="veh_passenger" depart="1259.45" departLane="best" departSpeed="max" from="449606871" to="31823236#1"/>
    <trip id="veh483" type="veh_passenger" depart="1262.07" departLane="best" departSpeed="max" from="449606871" to="-123214371#0"/>
    <trip id="veh484" type="veh_passenger" depart="1264.68" departLane="best" from="184444814#1" to="543400544#1"/>
    <trip id="veh485" type="veh_passenger" depart="1267.29" departLane="best" from="-122307616" to="23247531#9"/>
    <trip id="veh486" type="veh_passenger" depart="1269.91" departLane="best" from="-543748715" to="116315042"/>
    <trip id="veh487" type="veh_passenger" depart="1272.52" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#9"/>
    <trip id="veh489" type="veh_passenger" depart="1277.74" departLane="best" departSpeed="max" from="449606871" to="32036700#4"/>
    <trip id="veh490" type="veh_passenger" depart="1280.36" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh491" type="veh_passenger" depart="1282.97" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh494" type="veh_passenger" depart="1290.81" departLane="best" departSpeed="max" from="-543400544#1" to="-123214371#0"/>
    <trip id="veh496" type="veh_passenger" depart="1296.04" departLane="best" departSpeed="max" from="449606867" to="54856713"/>
    <trip id="veh497" type="veh_passenger" depart="1298.65" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh500" type="veh_passenger" depart="1306.49" departLane="best" from="23247531#0" to="23247531#9"/>
    <trip id="veh501" type="veh_passenger" depart="1309.10" departLane="best" departSpeed="max" from="-543400544#1" to="32036700#4"/>
    <trip id="veh502" type="veh_passenger" depart="1311.71" departLane="best" departSpeed="max" from="-543400544#1" to="32036700#4"/>
    <trip id="veh503" type="veh_passenger" depart="1314.33" departLane="best" departSpeed="max" from="449606871" to="54856713"/>
    <trip id="veh505" type="veh_passenger" depart="1319.55" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh507" type="veh_passenger" depart="1324.78" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh509" type="veh_passenger" depart="1330.00" departLane="best" departSpeed="max" from="-23492230#1" to="775343568#1"/>
    <trip id="veh511" type="veh_passenger" depart="1335.23" departLane="best" departSpeed="max" from="449606867" to="68506246#1"/>
    <trip id="veh513" type="veh_passenger" depart="1340.46" departLane="best" departSpeed="max" from="-262841306#2" to="32036700#4"/>
    <trip id="veh515" type="veh_passenger" depart="1345.68" departLane="best" departSpeed="max" from="-262841306#2" to="54856713"/>
    <trip id="veh516" type="veh_passenger" depart="1348.30" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh517" type="veh_passenger" depart="1350.91" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh519" type="veh_passenger" depart="1356.13" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh521" type="veh_passenger" depart="1361.36" departLane="best" from="-879196982#1" to="54856677#1"/>
    <trip id="veh524" type="veh_passenger" depart="1369.20" departLane="best" departSpeed="max" from="23492230#0" to="320838381#1"/>
    <trip id="veh526" type="veh_passenger" depart="1374.42" departLane="best" departSpeed="max" from="-87408929#4" to="543400544#1"/>
    <trip id="veh527" type="veh_passenger" depart="1377.04" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh528" type="veh_passenger" depart="1379.65" departLane="best" departSpeed="max" from="-262841306#2" to="116315042"/>
    <trip id="veh529" type="veh_passenger" depart="1382.26" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh530" type="veh_passenger" depart="1384.88" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh531" type="veh_passenger" depart="1387.49" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh532" type="veh_passenger" depart="1390.10" departLane="best" departSpeed="max" from="-87408929#4" to="543400544#1"/>
    <trip id="veh533" type="veh_passenger" depart="1392.72" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh534" type="veh_passenger" depart="1395.33" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh535" type="veh_passenger" depart="1397.94" departLane="best" from="129216265#0" to="543400544#1"/>
    <trip id="veh537" type="veh_passenger" depart="1403.17" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh539" type="veh_passenger" depart="1408.39" departLane="best" departSpeed="max" from="123214371#0" to="262841306#2"/>
    <trip id="veh540" type="veh_passenger" depart="1411.01" departLane="best" departSpeed="max" from="-543400544#1" to="122307620#0"/>
    <trip id="veh541" type="veh_passenger" depart="1413.62" departLane="best" departSpeed="max" from="68506239#0" to="-314417326"/>
    <trip id="veh542" type="veh_passenger" depart="1416.23" departLane="best" departSpeed="max" from="-54856713" to="116315042"/>
    <trip id="veh543" type="veh_passenger" depart="1418.85" departLane="best" departSpeed="max" from="-262841306#2" to="54856714#0"/>
    <trip id="veh545" type="veh_passenger" depart="1424.07" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh546" type="veh_passenger" depart="1426.68" departLane="best" departSpeed="max" from="-170958183#1" to="23247531#9"/>
    <trip id="veh547" type="veh_passenger" depart="1429.30" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh548" type="veh_passenger" depart="1431.91" departLane="best" departSpeed="max" from="123214371#0" to="116315042"/>
    <trip id="veh549" type="veh_passenger" depart="1434.52" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh550" type="veh_passenger" depart="1437.14" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh551" type="veh_passenger" depart="1439.75" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh554" type="veh_passenger" depart="1447.59" departLane="best" departSpeed="max" from="54856708#0" to="262841306#2"/>
    <trip id="veh555" type="veh_passenger" depart="1450.20" departLane="best" departSpeed="max" from="68506239#0" to="87408929#4"/>
    <trip id="veh556" type="veh_passenger" depart="1452.81" departLane="best" from="237914517#1" to="230423229"/>
    <trip id="veh557" type="veh_passenger" depart="1455.43" departLane="best" departSpeed="max" from="123214371#0" to="35549511#1"/>
    <trip id="veh558" type="veh_passenger" depart="1458.04" departLane="best" departSpeed="max" from="68506239#0" to="54856713"/>
    <trip id="veh559" type="veh_passenger" depart="1460.65" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh560" type="veh_passenger" depart="1463.27" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh561" type="veh_passenger" depart="1465.88" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh562" type="veh_passenger" depart="1468.49" departLane="best" departSpeed="max" from="-543400544#1" to="54856715#1"/>
    <trip id="veh563" type="veh_passenger" depart="1471.10" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh564" type="veh_passenger" depart="1473.72" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh565" type="veh_passenger" depart="1476.33" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh566" type="veh_passenger" depart="1478.94" departLane="best" departSpeed="max" from="-23492230#1" to="775343568#1"/>
    <trip id="veh568" type="veh_passenger" depart="1484.17" departLane="best" from="122307616" to="-123214371#0"/>
    <trip id="veh569" type="veh_passenger" depart="1486.78" departLane="best" from="237914517#1" to="32036700#4"/>
    <trip id="veh570" type="veh_passenger" depart="1489.40" departLane="best" departSpeed="max" from="-54856713" to="87408929#4"/>
    <trip id="veh571" type="veh_passenger" depart="1492.01" departLane="best" departSpeed="max" from="68506239#0" to="54856713"/>
    <trip id="veh573" type="veh_passenger" depart="1497.23" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#2"/>
    <trip id="veh574" type="veh_passenger" depart="1499.85" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh575" type="veh_passenger" depart="1502.46" departLane="best" departSpeed="max" from="23492230#0" to="122307615#0"/>
    <trip id="veh576" type="veh_passenger" depart="1505.07" departLane="best" departSpeed="max" from="-262841306#2" to="230423229"/>
    <trip id="veh577" type="veh_passenger" depart="1507.69" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh578" type="veh_passenger" depart="1510.30" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#4"/>
    <trip id="veh579" type="veh_passenger" depart="1512.91" departLane="best" departSpeed="max" from="-775343568#1" to="-123214371#0"/>
    <trip id="veh581" type="veh_passenger" depart="1518.14" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh582" type="veh_passenger" depart="1520.75" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh583" type="veh_passenger" depart="1523.36" departLane="best" from="-122307620#1" to="32036700#4"/>
    <trip id="veh584" type="veh_passenger" depart="1525.98" departLane="best" departSpeed="max" from="-54856713" to="116315042"/>
    <trip id="veh585" type="veh_passenger" depart="1528.59" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh587" type="veh_passenger" depart="1533.82" departLane="best" departSpeed="max" from="68506239#0" to="-543754801"/>
    <trip id="veh589" type="veh_passenger" depart="1539.04" departLane="best" from="830793055" to="35549511#1"/>
    <trip id="veh590" type="veh_passenger" depart="1541.66" departLane="best" departSpeed="max" from="449606867" to="87408929#4"/>
    <trip id="veh591" type="veh_passenger" depart="1544.27" departLane="best" departSpeed="max" from="-775343568#1" to="-122307620#0"/>
    <trip id="veh592" type="veh_passenger" depart="1546.88" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh593" type="veh_passenger" depart="1549.49" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh594" type="veh_passenger" depart="1552.11" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh595" type="veh_passenger" depart="1554.72" departLane="best" departSpeed="max" from="-87408929#4" to="-123214371#0"/>
    <trip id="veh599" type="veh_passenger" depart="1565.17" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh600" type="veh_passenger" depart="1567.78" departLane="best" departSpeed="max" from="-170958183#1" to="775343568#1"/>
    <trip id="veh604" type="veh_passenger" depart="1578.24" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh606" type="veh_passenger" depart="1583.46" departLane="best" departSpeed="max" from="68506239#0" to="26293951#1"/>
    <trip id="veh607" type="veh_passenger" depart="1586.08" departLane="best" departSpeed="max" from="68506239#0" to="32036700#4"/>
    <trip id="veh608" type="veh_passenger" depart="1588.69" departLane="best" departSpeed="max" from="449606871" to="-867328945#2"/>
    <trip id="veh609" type="veh_passenger" depart="1591.30" departLane="best" from="916493566#0" to="54856713"/>
    <trip id="veh611" type="veh_passenger" depart="1596.53" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh612" type="veh_passenger" depart="1599.14" departLane="best" from="122307618" to="-123214371#0"/>
    <trip id="veh613" type="veh_passenger" depart="1601.75" departLane="best" departSpeed="max" from="54856708#0" to="237522642"/>
    <trip id="veh614" type="veh_passenger" depart="1604.37" departLane="best" from="910000958" to="230423229"/>
    <trip id="veh615" type="veh_passenger" depart="1606.98" departLane="best" from="68506239#9" to="775343568#1"/>
    <trip id="veh617" type="veh_passenger" depart="1612.21" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh619" type="veh_passenger" depart="1617.43" departLane="best" departSpeed="max" from="123214371#0" to="262841306#2"/>
    <trip id="veh621" type="veh_passenger" depart="1622.66" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh625" type="veh_passenger" depart="1633.11" departLane="best" from="-775343568#0" to="116315042"/>
    <trip id="veh626" type="veh_passenger" depart="1635.72" departLane="best" departSpeed="max" from="449606871" to="32036700#4"/>
    <trip id="veh627" type="veh_passenger" depart="1638.34" departLane="best" departSpeed="max" from="123214371#0" to="396767222#0"/>
    <trip id="veh628" type="veh_passenger" depart="1640.95" departLane="best" departSpeed="max" from="-775343568#1" to="68506246#1"/>
    <trip id="veh631" type="veh_passenger" depart="1648.79" departLane="best" from="-35549512#4" to="54856677#1"/>
    <trip id="veh632" type="veh_passenger" depart="1651.40" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh633" type="veh_passenger" depart="1654.01" departLane="best" from="116801392#3" to="543400544#0"/>
    <trip id="veh635" type="veh_passenger" depart="1659.24" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh636" type="veh_passenger" depart="1661.85" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh637" type="veh_passenger" depart="1664.46" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh641" type="veh_passenger" depart="1674.92" departLane="best" from="32036701#2" to="23247531#9"/>
    <trip id="veh642" type="veh_passenger" depart="1677.53" departLane="best" from="916493566#5" to="54856677#1"/>
    <trip id="veh643" type="veh_passenger" depart="1680.14" departLane="best" departSpeed="max" from="54856708#0" to="68506264"/>
    <trip id="veh644" type="veh_passenger" depart="1682.76" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh645" type="veh_passenger" depart="1685.37" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh646" type="veh_passenger" depart="1687.98" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh647" type="veh_passenger" depart="1690.59" departLane="best" departSpeed="max" from="23492230#0" to="68506246#1"/>
    <trip id="veh648" type="veh_passenger" depart="1693.21" departLane="best" departSpeed="max" from="-262841306#2" to="116315042"/>
    <trip id="veh649" type="veh_passenger" depart="1695.82" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh650" type="veh_passenger" depart="1698.43" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh651" type="veh_passenger" depart="1701.05" departLane="best" from="-116315037#1" to="775343568#1"/>
    <trip id="veh652" type="veh_passenger" depart="1703.66" departLane="best" departSpeed="max" from="68506239#0" to="54856715#1"/>
    <trip id="veh653" type="veh_passenger" depart="1706.27" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh655" type="veh_passenger" depart="1711.50" departLane="best" departSpeed="max" from="-775343568#1" to="35549511#1"/>
    <trip id="veh657" type="veh_passenger" depart="1716.72" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh658" type="veh_passenger" depart="1719.34" departLane="best" departSpeed="max" from="449606867" to="54856677#1"/>
    <trip id="veh659" type="veh_passenger" depart="1721.95" departLane="best" from="-396767222#1" to="54856713"/>
    <trip id="veh660" type="veh_passenger" depart="1724.56" departLane="best" departSpeed="max" from="-170958183#1" to="-123214371#0"/>
    <trip id="veh661" type="veh_passenger" depart="1727.18" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh663" type="veh_passenger" depart="1732.40" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh665" type="veh_passenger" depart="1737.63" departLane="best" departSpeed="max" from="-775343568#1" to="830793052"/>
    <trip id="veh666" type="veh_passenger" depart="1740.24" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh667" type="veh_passenger" depart="1742.85" departLane="best" departSpeed="max" from="-262841306#2" to="-35549512#4"/>
    <trip id="veh668" type="veh_passenger" depart="1745.47" departLane="best" from="-87408929#1" to="54856677#1"/>
    <trip id="veh669" type="veh_passenger" depart="1748.08" departLane="best" departSpeed="max" from="449606871" to="68506246#1"/>
    <trip id="veh670" type="veh_passenger" depart="1750.69" departLane="best" from="68506239#10" to="116315042"/>
    <trip id="veh671" type="veh_passenger" depart="1753.31" departLane="best" departSpeed="max" from="123214371#0" to="396767222#0"/>
    <trip id="veh674" type="veh_passenger" depart="1761.15" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh675" type="veh_passenger" depart="1763.76" departLane="best" departSpeed="max" from="123214371#0" to="543400544#1"/>
    <trip id="veh676" type="veh_passenger" depart="1766.37" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh678" type="veh_passenger" depart="1771.60" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh680" type="veh_passenger" depart="1776.82" departLane="best" departSpeed="max" from="-775343568#1" to="-123214371#0"/>
    <trip id="veh681" type="veh_passenger" depart="1779.44" departLane="best" departSpeed="max" from="68506239#0" to="32036700#4"/>
    <trip id="veh682" type="veh_passenger" depart="1782.05" departLane="best" departSpeed="max" from="68506239#0" to="122307620#0"/>
    <trip id="veh683" type="veh_passenger" depart="1784.66" departLane="best" from="122307619#0" to="-123214371#0"/>
    <trip id="veh687" type="veh_passenger" depart="1795.11" departLane="best" departSpeed="max" from="23492230#0" to="68506246#1"/>
    <trip id="veh691" type="veh_passenger" depart="1805.57" departLane="best" departSpeed="max" from="-775343568#1" to="87408929#4"/>
    <trip id="veh693" type="veh_passenger" depart="1810.79" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh695" type="veh_passenger" depart="1816.02" departLane="best" departSpeed="max" from="-543400544#1" to="32036700#4"/>
    <trip id="veh697" type="veh_passenger" depart="1821.24" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh698" type="veh_passenger" depart="1823.86" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh700" type="veh_passenger" depart="1829.08" departLane="best" departSpeed="max" from="-87408929#4" to="543400544#1"/>
    <trip id="veh701" type="veh_passenger" depart="1831.70" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh702" type="veh_passenger" depart="1834.31" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh703" type="veh_passenger" depart="1836.92" departLane="best" departSpeed="max" from="-87408929#4" to="54856713"/>
    <trip id="veh705" type="veh_passenger" depart="1842.15" departLane="best" departSpeed="max" from="-775343568#1" to="54856713"/>
    <trip id="veh706" type="veh_passenger" depart="1844.76" departLane="best" from="68506239#9" to="543400544#1"/>
    <trip id="veh707" type="veh_passenger" depart="1847.37" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh708" type="veh_passenger" depart="1849.99" departLane="best" departSpeed="max" from="449606871" to="-123214371#0"/>
    <trip id="veh710" type="veh_passenger" depart="1855.21" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh713" type="veh_passenger" depart="1863.05" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh714" type="veh_passenger" depart="1865.66" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh715" type="veh_passenger" depart="1868.28" departLane="best" departSpeed="max" from="449606871" to="-123214371#0"/>
    <trip id="veh716" type="veh_passenger" depart="1870.89" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh717" type="veh_passenger" depart="1873.50" departLane="best" from="-122307620#2" to="23247531#9"/>
    <trip id="veh718" type="veh_passenger" depart="1876.12" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh720" type="veh_passenger" depart="1881.34" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh721" type="veh_passenger" depart="1883.95" departLane="best" departSpeed="max" from="449606871" to="396767222#1"/>
    <trip id="veh722" type="veh_passenger" depart="1886.57" departLane="best" departSpeed="max" from="-543400544#1" to="-122307615#1"/>
    <trip id="veh723" type="veh_passenger" depart="1889.18" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh725" type="veh_passenger" depart="1894.41" departLane="best" departSpeed="max" from="-87408929#4" to="-237914517#1"/>
    <trip id="veh726" type="veh_passenger" depart="1897.02" departLane="best" departSpeed="max" from="449606867" to="54856677#1"/>
    <trip id="veh727" type="veh_passenger" depart="1899.63" departLane="best" from="571426636#2" to="35814811#1"/>
    <trip id="veh728" type="veh_passenger" depart="1902.25" departLane="best" departSpeed="max" from="-54856713" to="87408929#4"/>
    <trip id="veh729" type="veh_passenger" depart="1904.86" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#6"/>
    <trip id="veh730" type="veh_passenger" depart="1907.47" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh732" type="veh_passenger" depart="1912.70" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh733" type="veh_passenger" depart="1915.31" departLane="best" departSpeed="max" from="449606871" to="262841306#2"/>
    <trip id="veh734" type="veh_passenger" depart="1917.92" departLane="best" from="68506239#9" to="54856713"/>
    <trip id="veh736" type="veh_passenger" depart="1923.15" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#2"/>
    <trip id="veh737" type="veh_passenger" depart="1925.76" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh738" type="veh_passenger" depart="1928.38" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh740" type="veh_passenger" depart="1933.60" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh741" type="veh_passenger" depart="1936.21" departLane="best" departSpeed="max" from="-54856713" to="54856677#1"/>
    <trip id="veh744" type="veh_passenger" depart="1944.05" departLane="best" departSpeed="max" from="-23492230#1" to="775343568#1"/>
    <trip id="veh746" type="veh_passenger" depart="1949.28" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#1"/>
    <trip id="veh747" type="veh_passenger" depart="1951.89" departLane="best" departSpeed="max" from="-87408929#4" to="543400544#1"/>
    <trip id="veh748" type="veh_passenger" depart="1954.51" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh750" type="veh_passenger" depart="1959.73" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh751" type="veh_passenger" depart="1962.34" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh752" type="veh_passenger" depart="1964.96" departLane="best" from="551933608#2" to="116315042"/>
    <trip id="veh753" type="veh_passenger" depart="1967.57" departLane="best" departSpeed="max" from="68506239#0" to="-543754801"/>
    <trip id="veh754" type="veh_passenger" depart="1970.18" departLane="best" from="23247531#2" to="32036700#4"/>
    <trip id="veh755" type="veh_passenger" depart="1972.80" departLane="best" departSpeed="max" from="68506239#0" to="-184444814#0"/>
    <trip id="veh756" type="veh_passenger" depart="1975.41" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh757" type="veh_passenger" depart="1978.02" departLane="best" from="129216265#0" to="543400544#0"/>
    <trip id="veh758" type="veh_passenger" depart="1980.63" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh759" type="veh_passenger" depart="1983.25" departLane="best" departSpeed="max" from="-775343568#1" to="87408929#4"/>
    <trip id="veh761" type="veh_passenger" depart="1988.47" departLane="best" departSpeed="max" from="68506239#0" to="-553278873#2"/>
    <trip id="veh762" type="veh_passenger" depart="1991.09" departLane="best" from="271567225#0" to="116315042"/>
    <trip id="veh763" type="veh_passenger" depart="1993.70" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh764" type="veh_passenger" depart="1996.31" departLane="best" from="23492283#0" to="230423229"/>
    <trip id="veh766" type="veh_passenger" depart="2001.54" departLane="best" departSpeed="max" from="54856708#0" to="543400544#0"/>
    <trip id="veh767" type="veh_passenger" depart="2004.15" departLane="best" from="-543400544#0" to="116315042"/>
    <trip id="veh768" type="veh_passenger" depart="2006.76" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh769" type="veh_passenger" depart="2009.38" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh770" type="veh_passenger" depart="2011.99" departLane="best" from="-34054350#3" to="54856677#1"/>
    <trip id="veh771" type="veh_passenger" depart="2014.60" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh774" type="veh_passenger" depart="2022.44" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh776" type="veh_passenger" depart="2027.67" departLane="best" departSpeed="max" from="449606871" to="396767222#1"/>
    <trip id="veh777" type="veh_passenger" depart="2030.28" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh778" type="veh_passenger" depart="2032.89" departLane="best" departSpeed="max" from="23492230#0" to="32036700#4"/>
    <trip id="veh779" type="veh_passenger" depart="2035.51" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh780" type="veh_passenger" depart="2038.12" departLane="best" from="830793057#1" to="116315042"/>
    <trip id="veh781" type="veh_passenger" depart="2040.73" departLane="best" departSpeed="max" from="449606867" to="122307620#3"/>
    <trip id="veh782" type="veh_passenger" depart="2043.35" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh783" type="veh_passenger" depart="2045.96" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh784" type="veh_passenger" depart="2048.57" departLane="best" departSpeed="max" from="23492230#0" to="35549511#1"/>
    <trip id="veh786" type="veh_passenger" depart="2053.80" departLane="best" departSpeed="max" from="449606867" to="122307620#2"/>
    <trip id="veh788" type="veh_passenger" depart="2059.02" departLane="best" departSpeed="max" from="54856708#0" to="1162513994#1"/>
    <trip id="veh789" type="veh_passenger" depart="2061.64" departLane="best" departSpeed="max" from="-262841306#2" to="35549511#1"/>
    <trip id="veh791" type="veh_passenger" depart="2066.86" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh793" type="veh_passenger" depart="2072.09" departLane="best" from="-396767222#1" to="32036700#4"/>
    <trip id="veh794" type="veh_passenger" depart="2074.70" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh796" type="veh_passenger" depart="2079.93" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh797" type="veh_passenger" depart="2082.54" departLane="best" from="23247531#0" to="54856713"/>
    <trip id="veh798" type="veh_passenger" depart="2085.15" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh799" type="veh_passenger" depart="2087.77" departLane="best" departSpeed="max" from="-170958183#1" to="23247531#9"/>
    <trip id="veh800" type="veh_passenger" depart="2090.38" departLane="best" departSpeed="max" from="-262841306#2" to="87408929#4"/>
    <trip id="veh801" type="veh_passenger" depart="2092.99" departLane="best" departSpeed="max" from="68506239#0" to="-549755456#2"/>
    <trip id="veh802" type="veh_passenger" depart="2095.61" departLane="best" departSpeed="max" from="68506239#0" to="-123214371#0"/>
    <trip id="veh804" type="veh_passenger" depart="2100.83" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh806" type="veh_passenger" depart="2106.06" departLane="best" from="871687213#2" to="262841306#2"/>
    <trip id="veh807" type="veh_passenger" depart="2108.67" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh808" type="veh_passenger" depart="2111.28" departLane="best" departSpeed="max" from="449606867" to="23247531#9"/>
    <trip id="veh809" type="veh_passenger" depart="2113.90" departLane="best" departSpeed="max" from="-543400544#1" to="35549511#1"/>
    <trip id="veh810" type="veh_passenger" depart="2116.51" departLane="best" departSpeed="max" from="-54856713" to="-116801392#1"/>
    <trip id="veh811" type="veh_passenger" depart="2119.12" departLane="best" departSpeed="max" from="-170958183#1" to="23247531#9"/>
    <trip id="veh813" type="veh_passenger" depart="2124.35" departLane="best" from="-68506264" to="23247531#9"/>
    <trip id="veh814" type="veh_passenger" depart="2126.96" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh815" type="veh_passenger" depart="2129.57" departLane="best" from="-122307620#0" to="116801392#3"/>
    <trip id="veh816" type="veh_passenger" depart="2132.19" departLane="best" departSpeed="max" from="-543400544#1" to="-123214371#0"/>
    <trip id="veh817" type="veh_passenger" depart="2134.80" departLane="best" from="34038367#2" to="54856677#1"/>
    <trip id="veh820" type="veh_passenger" depart="2142.64" departLane="best" departSpeed="max" from="-54856713" to="68506246#1"/>
    <trip id="veh821" type="veh_passenger" depart="2145.25" departLane="best" departSpeed="max" from="-262841306#2" to="32036700#4"/>
    <trip id="veh822" type="veh_passenger" depart="2147.87" departLane="best" departSpeed="max" from="-775343568#1" to="54856713"/>
    <trip id="veh823" type="veh_passenger" depart="2150.48" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh824" type="veh_passenger" depart="2153.09" departLane="best" from="34054350#1" to="54856677#1"/>
    <trip id="veh825" type="veh_passenger" depart="2155.70" departLane="best" departSpeed="max" from="-54856713" to="23247531#9"/>
    <trip id="veh826" type="veh_passenger" depart="2158.32" departLane="best" departSpeed="max" from="-262841306#2" to="775343568#1"/>
    <trip id="veh827" type="veh_passenger" depart="2160.93" departLane="best" from="54856715#1" to="68506246#1"/>
    <trip id="veh830" type="veh_passenger" depart="2168.77" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh831" type="veh_passenger" depart="2171.38" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh832" type="veh_passenger" depart="2174.00" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh835" type="veh_passenger" depart="2181.83" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh836" type="veh_passenger" depart="2184.45" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh839" type="veh_passenger" depart="2192.29" departLane="best" departSpeed="max" from="-54856713" to="116315042"/>
    <trip id="veh840" type="veh_passenger" depart="2194.90" departLane="best" departSpeed="max" from="449606867" to="775343564#0"/>
    <trip id="veh841" type="veh_passenger" depart="2197.51" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh842" type="veh_passenger" depart="2200.12" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh843" type="veh_passenger" depart="2202.74" departLane="best" from="391402482#1" to="68506246#1"/>
    <trip id="veh845" type="veh_passenger" depart="2207.96" departLane="best" departSpeed="max" from="449606871" to="35814811#4"/>
    <trip id="veh847" type="veh_passenger" depart="2213.19" departLane="best" departSpeed="max" from="-87408929#4" to="830793052"/>
    <trip id="veh848" type="veh_passenger" depart="2215.80" departLane="best" from="-122307620#0" to="775343568#1"/>
    <trip id="veh849" type="veh_passenger" depart="2218.42" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh851" type="veh_passenger" depart="2223.64" departLane="best" departSpeed="max" from="-54856713" to="543400544#1"/>
    <trip id="veh852" type="veh_passenger" depart="2226.25" departLane="best" departSpeed="max" from="123214371#0" to="35549511#1"/>
    <trip id="veh853" type="veh_passenger" depart="2228.87" departLane="best" from="-122307620#2" to="230423229"/>
    <trip id="veh855" type="veh_passenger" depart="2234.09" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh857" type="veh_passenger" depart="2239.32" departLane="best" departSpeed="max" from="68506239#0" to="262841306#2"/>
    <trip id="veh858" type="veh_passenger" depart="2241.93" departLane="best" departSpeed="max" from="23492230#0" to="396767222#1"/>
    <trip id="veh859" type="veh_passenger" depart="2244.55" departLane="best" departSpeed="max" from="-54856713" to="116315042"/>
    <trip id="veh860" type="veh_passenger" depart="2247.16" departLane="best" departSpeed="max" from="32024073" to="30014919"/>
    <trip id="veh861" type="veh_passenger" depart="2249.77" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh863" type="veh_passenger" depart="2255.00" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh864" type="veh_passenger" depart="2257.61" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh865" type="veh_passenger" depart="2260.22" departLane="best" departSpeed="max" from="449606867" to="32036700#4"/>
    <trip id="veh867" type="veh_passenger" depart="2265.45" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh870" type="veh_passenger" depart="2273.29" departLane="best" departSpeed="max" from="-262841306#2" to="-123214371#0"/>
    <trip id="veh871" type="veh_passenger" depart="2275.90" departLane="best" departSpeed="max" from="54856708#0" to="775343568#1"/>
    <trip id="veh873" type="veh_passenger" depart="2281.13" departLane="best" departSpeed="max" from="-23492230#1" to="54856713"/>
    <trip id="veh874" type="veh_passenger" depart="2283.74" departLane="best" departSpeed="max" from="-54856713" to="54856677#1"/>
    <trip id="veh875" type="veh_passenger" depart="2286.35" departLane="best" from="129216265#0" to="32036700#4"/>
    <trip id="veh876" type="veh_passenger" depart="2288.97" departLane="best" departSpeed="max" from="123214371#0" to="543400544#1"/>
    <trip id="veh877" type="veh_passenger" depart="2291.58" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh879" type="veh_passenger" depart="2296.80" departLane="best" from="-116315037#1" to="35549511#1"/>
    <trip id="veh880" type="veh_passenger" depart="2299.42" departLane="best" departSpeed="max" from="-775343568#1" to="87408929#4"/>
    <trip id="veh883" type="veh_passenger" depart="2307.26" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh884" type="veh_passenger" depart="2309.87" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh885" type="veh_passenger" depart="2312.48" departLane="best" departSpeed="max" from="-543400544#1" to="35549511#1"/>
    <trip id="veh886" type="veh_passenger" depart="2315.10" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh887" type="veh_passenger" depart="2317.71" departLane="best" from="23247531#2" to="262841306#2"/>
    <trip id="veh888" type="veh_passenger" depart="2320.32" departLane="best" departSpeed="max" from="23492230#0" to="538552772#0"/>
    <trip id="veh890" type="veh_passenger" depart="2325.55" departLane="best" departSpeed="max" from="449606871" to="551933607#1"/>
    <trip id="veh891" type="veh_passenger" depart="2328.16" departLane="best" departSpeed="max" from="449606867" to="68506239#9"/>
    <trip id="veh892" type="veh_passenger" depart="2330.77" departLane="best" from="54856674#1" to="116315042"/>
    <trip id="veh893" type="veh_passenger" depart="2333.39" departLane="best" departSpeed="max" from="123214371#0" to="35549511#1"/>
    <trip id="veh894" type="veh_passenger" depart="2336.00" departLane="best" departSpeed="max" from="-262841306#2" to="-122307617"/>
    <trip id="veh895" type="veh_passenger" depart="2338.61" departLane="best" departSpeed="max" from="-543400544#1" to="32036700#4"/>
    <trip id="veh898" type="veh_passenger" depart="2346.45" departLane="best" departSpeed="max" from="-543400544#1" to="35549511#1"/>
    <trip id="veh899" type="veh_passenger" depart="2349.06" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh901" type="veh_passenger" depart="2354.29" departLane="best" departSpeed="max" from="-262841306#2" to="775343568#1"/>
    <trip id="veh902" type="veh_passenger" depart="2356.90" departLane="best" departSpeed="max" from="68506239#0" to="396767222#1"/>
    <trip id="veh903" type="veh_passenger" depart="2359.52" departLane="best" departSpeed="max" from="-170958183#1" to="775343568#1"/>
    <trip id="veh904" type="veh_passenger" depart="2362.13" departLane="best" departSpeed="max" from="-87408929#4" to="-184444814#0"/>
    <trip id="veh906" type="veh_passenger" depart="2367.36" departLane="best" departSpeed="max" from="-262841306#2" to="116315042"/>
    <trip id="veh907" type="veh_passenger" depart="2369.97" departLane="best" departSpeed="max" from="123214371#0" to="68506246#1"/>
    <trip id="veh909" type="veh_passenger" depart="2375.19" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh910" type="veh_passenger" depart="2377.81" departLane="best" departSpeed="max" from="449606871" to="32036700#4"/>
    <trip id="veh911" type="veh_passenger" depart="2380.42" departLane="best" departSpeed="max" from="68506239#0" to="-23492283#1"/>
    <trip id="veh912" type="veh_passenger" depart="2383.03" departLane="best" departSpeed="max" from="-23492230#1" to="54856713"/>
    <trip id="veh915" type="veh_passenger" depart="2390.87" departLane="best" departSpeed="max" from="-775343568#1" to="87408929#4"/>
    <trip id="veh918" type="veh_passenger" depart="2398.71" departLane="best" departSpeed="max" from="449606871" to="543400545"/>
    <trip id="veh919" type="veh_passenger" depart="2401.32" departLane="best" from="32036701#1" to="543400544#1"/>
    <trip id="veh920" type="veh_passenger" depart="2403.94" departLane="best" departSpeed="max" from="-262841306#2" to="116315042"/>
    <trip id="veh921" type="veh_passenger" depart="2406.55" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh922" type="veh_passenger" depart="2409.16" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh923" type="veh_passenger" depart="2411.78" departLane="best" departSpeed="max" from="-543400544#1" to="-116801392#4"/>
    <trip id="veh924" type="veh_passenger" depart="2414.39" departLane="best" departSpeed="max" from="-775343568#1" to="262841306#2"/>
    <trip id="veh925" type="veh_passenger" depart="2417.00" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh927" type="veh_passenger" depart="2422.23" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh928" type="veh_passenger" depart="2424.84" departLane="best" departSpeed="max" from="-170958183#1" to="-237914517#1"/>
    <trip id="veh929" type="veh_passenger" depart="2427.45" departLane="best" departSpeed="max" from="-87408929#4" to="262841306#2"/>
    <trip id="veh930" type="veh_passenger" depart="2430.07" departLane="best" departSpeed="max" from="-775343568#1" to="26293951#3"/>
    <trip id="veh931" type="veh_passenger" depart="2432.68" departLane="best" from="32023368#1" to="68506246#1"/>
    <trip id="veh932" type="veh_passenger" depart="2435.29" departLane="best" departSpeed="max" from="68506239#0" to="87408929#4"/>
    <trip id="veh934" type="veh_passenger" depart="2440.52" departLane="best" departSpeed="max" from="123214371#0" to="116315042"/>
    <trip id="veh937" type="veh_passenger" depart="2448.36" departLane="best" from="-571426636#2" to="543400544#1"/>
    <trip id="veh938" type="veh_passenger" depart="2450.97" departLane="best" from="32036704#0" to="116315042"/>
    <trip id="veh939" type="veh_passenger" depart="2453.58" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh941" type="veh_passenger" depart="2458.81" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh942" type="veh_passenger" depart="2461.42" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh943" type="veh_passenger" depart="2464.04" departLane="best" departSpeed="max" from="-87408929#4" to="775343568#1"/>
    <trip id="veh944" type="veh_passenger" depart="2466.65" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh945" type="veh_passenger" depart="2469.26" departLane="best" departSpeed="max" from="68506239#0" to="87408929#4"/>
    <trip id="veh946" type="veh_passenger" depart="2471.87" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh948" type="veh_passenger" depart="2477.10" departLane="best" departSpeed="max" from="-54856713" to="262841306#2"/>
    <trip id="veh949" type="veh_passenger" depart="2479.71" departLane="best" from="32036701#0" to="35549511#1"/>
    <trip id="veh952" type="veh_passenger" depart="2487.55" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh953" type="veh_passenger" depart="2490.17" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh954" type="veh_passenger" depart="2492.78" departLane="best" departSpeed="max" from="23492230#0" to="32036700#4"/>
    <trip id="veh955" type="veh_passenger" depart="2495.39" departLane="best" departSpeed="max" from="449606867" to="54856677#1"/>
    <trip id="veh956" type="veh_passenger" depart="2498.00" departLane="best" departSpeed="max" from="54856708#0" to="68506264"/>
    <trip id="veh957" type="veh_passenger" depart="2500.62" departLane="best" departSpeed="max" from="23492230#0" to="32036700#4"/>
    <trip id="veh958" type="veh_passenger" depart="2503.23" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh959" type="veh_passenger" depart="2505.84" departLane="best" from="-122307620#2" to="68506246#1"/>
    <trip id="veh962" type="veh_passenger" depart="2513.68" departLane="best" departSpeed="max" from="449606867" to="262841306#1"/>
    <trip id="veh963" type="veh_passenger" depart="2516.29" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh964" type="veh_passenger" depart="2518.91" departLane="best" from="775343564#1" to="54856677#1"/>
    <trip id="veh970" type="veh_passenger" depart="2534.59" departLane="best" departSpeed="max" from="23492230#0" to="271567225#0"/>
    <trip id="veh971" type="veh_passenger" depart="2537.20" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh973" type="veh_passenger" depart="2542.42" departLane="best" departSpeed="max" from="23492230#0" to="867340083"/>
    <trip id="veh974" type="veh_passenger" depart="2545.04" departLane="best" departSpeed="max" from="-543400544#1" to="775343568#1"/>
    <trip id="veh975" type="veh_passenger" depart="2547.65" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh976" type="veh_passenger" depart="2550.26" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh977" type="veh_passenger" depart="2552.88" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh978" type="veh_passenger" depart="2555.49" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh980" type="veh_passenger" depart="2560.72" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh981" type="veh_passenger" depart="2563.33" departLane="best" from="314417325#2" to="830793057#0"/>
    <trip id="veh983" type="veh_passenger" depart="2568.55" departLane="best" departSpeed="max" from="449606871" to="116801392#2"/>
    <trip id="veh985" type="veh_passenger" depart="2573.78" departLane="best" departSpeed="max" from="-262841306#2" to="116315042"/>
    <trip id="veh986" type="veh_passenger" depart="2576.39" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh988" type="veh_passenger" depart="2581.62" departLane="best" departSpeed="max" from="-775343568#1" to="68506246#1"/>
    <trip id="veh990" type="veh_passenger" depart="2586.85" departLane="best" from="34038366#2" to="543400544#1"/>
    <trip id="veh992" type="veh_passenger" depart="2592.07" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh993" type="veh_passenger" depart="2594.68" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh994" type="veh_passenger" depart="2597.30" departLane="best" from="32023368#1" to="116315042"/>
    <trip id="veh995" type="veh_passenger" depart="2599.91" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh996" type="veh_passenger" depart="2602.52" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh997" type="veh_passenger" depart="2605.14" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh998" type="veh_passenger" depart="2607.75" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh999" type="veh_passenger" depart="2610.36" departLane="best" from="916493566#5" to="35549511#1"/>
    <trip id="veh1002" type="veh_passenger" depart="2618.20" departLane="best" from="-122307617" to="116315042"/>
    <trip id="veh1003" type="veh_passenger" depart="2620.81" departLane="best" departSpeed="max" from="54856708#0" to="87408929#1"/>
    <trip id="veh1006" type="veh_passenger" depart="2628.65" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh1007" type="veh_passenger" depart="2631.27" departLane="best" departSpeed="max" from="-262841306#2" to="-123214371#0"/>
    <trip id="veh1008" type="veh_passenger" depart="2633.88" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1009" type="veh_passenger" depart="2636.49" departLane="best" departSpeed="max" from="-775343568#1" to="35549511#1"/>
    <trip id="veh1014" type="veh_passenger" depart="2649.56" departLane="best" departSpeed="max" from="54856708#0" to="262841306#2"/>
    <trip id="veh1015" type="veh_passenger" depart="2652.17" departLane="best" from="830793057#1" to="116315042"/>
    <trip id="veh1016" type="veh_passenger" depart="2654.78" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh1017" type="veh_passenger" depart="2657.40" departLane="best" departSpeed="max" from="-775343568#1" to="916493566#4"/>
    <trip id="veh1018" type="veh_passenger" depart="2660.01" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh1022" type="veh_passenger" depart="2670.46" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh1023" type="veh_passenger" depart="2673.07" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1024" type="veh_passenger" depart="2675.69" departLane="best" departSpeed="max" from="-543400544#1" to="-122307615#1"/>
    <trip id="veh1025" type="veh_passenger" depart="2678.30" departLane="best" departSpeed="max" from="-262841306#2" to="-23492230#2"/>
    <trip id="veh1026" type="veh_passenger" depart="2680.91" departLane="best" departSpeed="max" from="-775343568#1" to="35549511#1"/>
    <trip id="veh1027" type="veh_passenger" depart="2683.53" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh1028" type="veh_passenger" depart="2686.14" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh1030" type="veh_passenger" depart="2691.36" departLane="best" departSpeed="max" from="68506239#0" to="87408929#4"/>
    <trip id="veh1031" type="veh_passenger" depart="2693.98" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh1033" type="veh_passenger" depart="2699.20" departLane="best" departSpeed="max" from="449606871" to="54856713"/>
    <trip id="veh1036" type="veh_passenger" depart="2707.04" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh1037" type="veh_passenger" depart="2709.65" departLane="best" from="549755456#2" to="116315042"/>
    <trip id="veh1038" type="veh_passenger" depart="2712.27" departLane="best" departSpeed="max" from="68506239#0" to="262841306#2"/>
    <trip id="veh1041" type="veh_passenger" depart="2720.11" departLane="best" from="-122307620#2" to="396767222#1"/>
    <trip id="veh1043" type="veh_passenger" depart="2725.33" departLane="best" departSpeed="max" from="23492230#0" to="-122307619#0"/>
    <trip id="veh1044" type="veh_passenger" depart="2727.95" departLane="best" departSpeed="max" from="-775343568#1" to="54856713"/>
    <trip id="veh1045" type="veh_passenger" depart="2730.56" departLane="best" departSpeed="max" from="-87408929#4" to="543400544#1"/>
    <trip id="veh1046" type="veh_passenger" depart="2733.17" departLane="best" from="23492283#2" to="54856677#1"/>
    <trip id="veh1047" type="veh_passenger" depart="2735.78" departLane="best" departSpeed="max" from="-543400544#1" to="68506239#8"/>
    <trip id="veh1048" type="veh_passenger" depart="2738.40" departLane="best" departSpeed="max" from="23492230#0" to="-1162513994#0"/>
    <trip id="veh1049" type="veh_passenger" depart="2741.01" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1050" type="veh_passenger" depart="2743.62" departLane="best" departSpeed="max" from="68506239#0" to="230423229"/>
    <trip id="veh1051" type="veh_passenger" depart="2746.24" departLane="best" departSpeed="max" from="123214371#0" to="68506246#1"/>
    <trip id="veh1053" type="veh_passenger" depart="2751.46" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh1054" type="veh_passenger" depart="2754.08" departLane="best" departSpeed="max" from="-54856713" to="543400544#1"/>
    <trip id="veh1055" type="veh_passenger" depart="2756.69" departLane="best" departSpeed="max" from="449606871" to="-1162513994#1"/>
    <trip id="veh1057" type="veh_passenger" depart="2761.91" departLane="best" departSpeed="max" from="-54856713" to="54856677#1"/>
    <trip id="veh1058" type="veh_passenger" depart="2764.53" departLane="best" from="129216265#0" to="54856677#1"/>
    <trip id="veh1059" type="veh_passenger" depart="2767.14" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh1060" type="veh_passenger" depart="2769.75" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh1061" type="veh_passenger" depart="2772.37" departLane="best" departSpeed="max" from="-87408929#4" to="68506246#1"/>
    <trip id="veh1063" type="veh_passenger" depart="2777.59" departLane="best" from="871687213#2" to="116315042"/>
    <trip id="veh1066" type="veh_passenger" depart="2785.43" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh1068" type="veh_passenger" depart="2790.66" departLane="best" departSpeed="max" from="449606871" to="68506246#1"/>
    <trip id="veh1069" type="veh_passenger" depart="2793.27" departLane="best" from="32036701#3" to="116315042"/>
    <trip id="veh1070" type="veh_passenger" depart="2795.88" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh1071" type="veh_passenger" depart="2798.50" departLane="best" departSpeed="max" from="123214371#0" to="129648702#0"/>
    <trip id="veh1072" type="veh_passenger" depart="2801.11" departLane="best" departSpeed="max" from="-87408929#4" to="116315042"/>
    <trip id="veh1073" type="veh_passenger" depart="2803.72" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh1076" type="veh_passenger" depart="2811.56" departLane="best" departSpeed="max" from="449606871" to="551933607#1"/>
    <trip id="veh1077" type="veh_passenger" depart="2814.17" departLane="best" departSpeed="max" from="449606867" to="23247531#9"/>
    <trip id="veh1079" type="veh_passenger" depart="2819.40" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh1080" type="veh_passenger" depart="2822.01" departLane="best" departSpeed="max" from="68506239#0" to="32036700#4"/>
    <trip id="veh1081" type="veh_passenger" depart="2824.63" departLane="best" departSpeed="max" from="-543400544#1" to="32036700#4"/>
    <trip id="veh1082" type="veh_passenger" depart="2827.24" departLane="best" from="32036704#0" to="35549511#1"/>
    <trip id="veh1083" type="veh_passenger" depart="2829.85" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1084" type="veh_passenger" depart="2832.46" departLane="best" departSpeed="max" from="54856708#0" to="543400544#0"/>
    <trip id="veh1085" type="veh_passenger" depart="2835.08" departLane="best" departSpeed="max" from="54856708#0" to="775343569#0"/>
    <trip id="veh1086" type="veh_passenger" depart="2837.69" departLane="best" departSpeed="max" from="54856708#0" to="-122307620#0"/>
    <trip id="veh1087" type="veh_passenger" depart="2840.30" departLane="best" departSpeed="max" from="-262841306#2" to="230423229"/>
    <trip id="veh1088" type="veh_passenger" depart="2842.92" departLane="best" departSpeed="max" from="-170958183#1" to="396767222#0"/>
    <trip id="veh1089" type="veh_passenger" depart="2845.53" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh1090" type="veh_passenger" depart="2848.14" departLane="best" departSpeed="max" from="449606867" to="32036700#4"/>
    <trip id="veh1091" type="veh_passenger" depart="2850.76" departLane="best" departSpeed="max" from="-262841306#2" to="35549511#1"/>
    <trip id="veh1092" type="veh_passenger" depart="2853.37" departLane="best" departSpeed="max" from="449606867" to="230423229"/>
    <trip id="veh1093" type="veh_passenger" depart="2855.98" departLane="best" from="830793057#0" to="32036700#4"/>
    <trip id="veh1096" type="veh_passenger" depart="2863.82" departLane="best" departSpeed="max" from="449606867" to="54856713"/>
    <trip id="veh1097" type="veh_passenger" depart="2866.43" departLane="best" departSpeed="max" from="449606871" to="543400545"/>
    <trip id="veh1099" type="veh_passenger" depart="2871.66" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh1101" type="veh_passenger" depart="2876.89" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh1102" type="veh_passenger" depart="2879.50" departLane="best" departSpeed="max" from="-543400544#1" to="775343568#1"/>
    <trip id="veh1104" type="veh_passenger" depart="2884.72" departLane="best" departSpeed="max" from="-775343568#1" to="68506246#1"/>
    <trip id="veh1105" type="veh_passenger" depart="2887.34" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh1106" type="veh_passenger" depart="2889.95" departLane="best" departSpeed="max" from="-262841306#2" to="54856677#1"/>
    <trip id="veh1108" type="veh_passenger" depart="2895.18" departLane="best" departSpeed="max" from="23492230#0" to="262841306#2"/>
    <trip id="veh1110" type="veh_passenger" depart="2900.40" departLane="best" departSpeed="max" from="123214371#0" to="184444814#1"/>
    <trip id="veh1114" type="veh_passenger" depart="2910.85" departLane="best" departSpeed="max" from="-543400544#1" to="775343568#1"/>
    <trip id="veh1115" type="veh_passenger" depart="2913.47" departLane="best" departSpeed="max" from="54856708#0" to="35549511#1"/>
    <trip id="veh1117" type="veh_passenger" depart="2918.69" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh1118" type="veh_passenger" depart="2921.31" departLane="best" departSpeed="max" from="-775343568#1" to="-830793058"/>
    <trip id="veh1119" type="veh_passenger" depart="2923.92" departLane="best" departSpeed="max" from="123214371#0" to="32036700#4"/>
    <trip id="veh1120" type="veh_passenger" depart="2926.53" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1121" type="veh_passenger" depart="2929.14" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh1122" type="veh_passenger" depart="2931.76" departLane="best" departSpeed="max" from="68506239#0" to="-543754801"/>
    <trip id="veh1123" type="veh_passenger" depart="2934.37" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1125" type="veh_passenger" depart="2939.60" departLane="best" departSpeed="max" from="-775343568#1" to="543400544#1"/>
    <trip id="veh1126" type="veh_passenger" depart="2942.21" departLane="best" from="26293951#1" to="543400544#1"/>
    <trip id="veh1129" type="veh_passenger" depart="2950.05" departLane="best" departSpeed="max" from="-54856713" to="23247531#9"/>
    <trip id="veh1130" type="veh_passenger" depart="2952.66" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh1131" type="veh_passenger" depart="2955.27" departLane="best" departSpeed="max" from="68506239#0" to="116315037#1"/>
    <trip id="veh1132" type="veh_passenger" depart="2957.89" departLane="best" departSpeed="max" from="449606867" to="552634550#0"/>
    <trip id="veh1135" type="veh_passenger" depart="2965.73" departLane="best" from="887909226#1" to="54856677#1"/>
    <trip id="veh1136" type="veh_passenger" depart="2968.34" departLane="best" departSpeed="max" from="68506239#0" to="-32036701#2"/>
    <trip id="veh1137" type="veh_passenger" depart="2970.95" departLane="best" departSpeed="max" from="449606871" to="262841306#2"/>
    <trip id="veh1138" type="veh_passenger" depart="2973.57" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1139" type="veh_passenger" depart="2976.18" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1140" type="veh_passenger" depart="2978.79" departLane="best" departSpeed="max" from="54856708#0" to="775343568#0"/>
    <trip id="veh1141" type="veh_passenger" depart="2981.40" departLane="best" departSpeed="max" from="32024073" to="30014919"/>
    <trip id="veh1143" type="veh_passenger" depart="2986.63" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#9"/>
    <trip id="veh1144" type="veh_passenger" depart="2989.24" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh1148" type="veh_passenger" depart="2999.70" departLane="best" departSpeed="max" from="23492230#0" to="35549511#1"/>
    <trip id="veh1149" type="veh_passenger" depart="3002.31" departLane="best" departSpeed="max" from="68506239#0" to="122307615#0"/>
    <trip id="veh1150" type="veh_passenger" depart="3004.92" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1151" type="veh_passenger" depart="3007.53" departLane="best" departSpeed="max" from="449606871" to="54856713"/>
    <trip id="veh1152" type="veh_passenger" depart="3010.15" departLane="best" departSpeed="max" from="449606871" to="-123214371#0"/>
    <trip id="veh1153" type="veh_passenger" depart="3012.76" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1155" type="veh_passenger" depart="3017.99" departLane="best" from="-122307620#0" to="32036700#4"/>
    <trip id="veh1156" type="veh_passenger" depart="3020.60" departLane="best" departSpeed="max" from="449606871" to="230423229"/>
    <trip id="veh1157" type="veh_passenger" depart="3023.21" departLane="best" from="-867328945#1" to="-123214371#0"/>
    <trip id="veh1159" type="veh_passenger" depart="3028.44" departLane="best" departSpeed="max" from="-543400544#1" to="-122307618"/>
    <trip id="veh1160" type="veh_passenger" depart="3031.05" departLane="best" departSpeed="max" from="54856708#0" to="87408929#4"/>
    <trip id="veh1161" type="veh_passenger" depart="3033.66" departLane="best" departSpeed="max" from="-543400544#1" to="54856713"/>
    <trip id="veh1162" type="veh_passenger" depart="3036.28" departLane="best" departSpeed="max" from="123214371#0" to="543400544#1"/>
    <trip id="veh1163" type="veh_passenger" depart="3038.89" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh1164" type="veh_passenger" depart="3041.50" departLane="best" departSpeed="max" from="-170958183#1" to="543400544#0"/>
    <trip id="veh1166" type="veh_passenger" depart="3046.73" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1168" type="veh_passenger" depart="3051.95" departLane="best" departSpeed="max" from="-170958183#1" to="54856677#1"/>
    <trip id="veh1169" type="veh_passenger" depart="3054.57" departLane="best" departSpeed="max" from="-543400544#1" to="122307620#0"/>
    <trip id="veh1170" type="veh_passenger" depart="3057.18" departLane="best" departSpeed="max" from="-543400544#1" to="230423229"/>
    <trip id="veh1173" type="veh_passenger" depart="3065.02" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh1175" type="veh_passenger" depart="3070.25" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh1179" type="veh_passenger" depart="3080.70" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh1180" type="veh_passenger" depart="3083.31" departLane="best" departSpeed="max" from="-54856713" to="68506246#1"/>
    <trip id="veh1181" type="veh_passenger" depart="3085.92" departLane="best" from="867328945#2" to="35549511#1"/>
    <trip id="veh1182" type="veh_passenger" depart="3088.54" departLane="best" departSpeed="max" from="54856708#0" to="35814811#3"/>
    <trip id="veh1183" type="veh_passenger" depart="3091.15" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1184" type="veh_passenger" depart="3093.76" departLane="best" departSpeed="max" from="449606867" to="230423229"/>
    <trip id="veh1185" type="veh_passenger" depart="3096.38" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh1186" type="veh_passenger" depart="3098.99" departLane="best" departSpeed="max" from="-262841306#2" to="879196982#1"/>
    <trip id="veh1187" type="veh_passenger" depart="3101.60" departLane="best" departSpeed="max" from="-54856713" to="549755456#2"/>
    <trip id="veh1188" type="veh_passenger" depart="3104.21" departLane="best" departSpeed="max" from="54856708#0" to="-830793058"/>
    <trip id="veh1189" type="veh_passenger" depart="3106.83" departLane="best" departSpeed="max" from="54856708#0" to="54856715#0"/>
    <trip id="veh1190" type="veh_passenger" depart="3109.44" departLane="best" departSpeed="max" from="68506239#0" to="-123214371#0"/>
    <trip id="veh1191" type="veh_passenger" depart="3112.05" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh1193" type="veh_passenger" depart="3117.28" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh1194" type="veh_passenger" depart="3119.89" departLane="best" from="549755456#3" to="32036700#4"/>
    <trip id="veh1195" type="veh_passenger" depart="3122.50" departLane="best" from="877826826" to="32036700#4"/>
    <trip id="veh1197" type="veh_passenger" depart="3127.73" departLane="best" departSpeed="max" from="-54856713" to="35549511#1"/>
    <trip id="veh1198" type="veh_passenger" depart="3130.34" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1199" type="veh_passenger" depart="3132.96" departLane="best" departSpeed="max" from="449606871" to="68506246#1"/>
    <trip id="veh1200" type="veh_passenger" depart="3135.57" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1201" type="veh_passenger" depart="3138.18" departLane="best" departSpeed="max" from="-170958183#1" to="543400544#1"/>
    <trip id="veh1203" type="veh_passenger" depart="3143.41" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh1204" type="veh_passenger" depart="3146.02" departLane="best" departSpeed="max" from="68506239#0" to="262841306#2"/>
    <trip id="veh1206" type="veh_passenger" depart="3151.25" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh1207" type="veh_passenger" depart="3153.86" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1209" type="veh_passenger" depart="3159.09" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh1210" type="veh_passenger" depart="3161.70" departLane="best" departSpeed="max" from="-543400544#1" to="87408929#4"/>
    <trip id="veh1211" type="veh_passenger" depart="3164.31" departLane="best" departSpeed="max" from="449606871" to="775343568#1"/>
    <trip id="veh1212" type="veh_passenger" depart="3166.93" departLane="best" departSpeed="max" from="54856708#0" to="129648702#4"/>
    <trip id="veh1214" type="veh_passenger" depart="3172.15" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh1216" type="veh_passenger" depart="3177.38" departLane="best" departSpeed="max" from="68506239#0" to="775343568#1"/>
    <trip id="veh1217" type="veh_passenger" depart="3179.99" departLane="best" from="129216265#0" to="87408929#4"/>
    <trip id="veh1218" type="veh_passenger" depart="3182.60" departLane="best" departSpeed="max" from="68506239#0" to="-123214371#0"/>
    <trip id="veh1219" type="veh_passenger" depart="3185.22" departLane="best" departSpeed="max" from="68506239#0" to="-122307617"/>
    <trip id="veh1222" type="veh_passenger" depart="3193.06" departLane="best" departSpeed="max" from="-54856713" to="87408929#4"/>
    <trip id="veh1223" type="veh_passenger" depart="3195.67" departLane="best" from="-396767222#0" to="54856677#1"/>
    <trip id="veh1224" type="veh_passenger" depart="3198.28" departLane="best" departSpeed="max" from="449606867" to="68506246#1"/>
    <trip id="veh1225" type="veh_passenger" depart="3200.89" departLane="best" departSpeed="max" from="-23492230#1" to="230423229"/>
    <trip id="veh1226" type="veh_passenger" depart="3203.51" departLane="best" from="-775343568#0" to="-543754801"/>
    <trip id="veh1227" type="veh_passenger" depart="3206.12" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh1228" type="veh_passenger" depart="3208.73" departLane="best" from="-116315037#1" to="26293951#0"/>
    <trip id="veh1229" type="veh_passenger" depart="3211.35" departLane="best" departSpeed="max" from="449606871" to="87408929#4"/>
    <trip id="veh1230" type="veh_passenger" depart="3213.96" departLane="best" from="553278873#1" to="32036700#4"/>
    <trip id="veh1231" type="veh_passenger" depart="3216.57" departLane="best" departSpeed="max" from="123214371#0" to="35549511#1"/>
    <trip id="veh1233" type="veh_passenger" depart="3221.80" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh1234" type="veh_passenger" depart="3224.41" departLane="best" departSpeed="max" from="-262841306#2" to="54856715#1"/>
    <trip id="veh1235" type="veh_passenger" depart="3227.02" departLane="best" departSpeed="max" from="-775343568#1" to="23247531#9"/>
    <trip id="veh1236" type="veh_passenger" depart="3229.64" departLane="best" departSpeed="max" from="68506239#0" to="775343568#0"/>
    <trip id="veh1237" type="veh_passenger" depart="3232.25" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1238" type="veh_passenger" depart="3234.86" departLane="best" from="32036704#0" to="543400544#1"/>
    <trip id="veh1240" type="veh_passenger" depart="3240.09" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1241" type="veh_passenger" depart="3242.70" departLane="best" departSpeed="max" from="68506239#0" to="116315042"/>
    <trip id="veh1242" type="veh_passenger" depart="3245.31" departLane="best" departSpeed="max" from="-543400544#1" to="116801392#1"/>
    <trip id="veh1243" type="veh_passenger" depart="3247.93" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh1245" type="veh_passenger" depart="3253.15" departLane="best" departSpeed="max" from="23492230#0" to="23247531#9"/>
    <trip id="veh1246" type="veh_passenger" depart="3255.77" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh1247" type="veh_passenger" depart="3258.38" departLane="best" departSpeed="max" from="-543400544#1" to="-123214371#0"/>
    <trip id="veh1248" type="veh_passenger" depart="3260.99" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh1249" type="veh_passenger" depart="3263.61" departLane="best" departSpeed="max" from="-262841306#2" to="54856713"/>
    <trip id="veh1252" type="veh_passenger" depart="3271.44" departLane="best" from="-396767222#1" to="35549511#1"/>
    <trip id="veh1255" type="veh_passenger" depart="3279.28" departLane="best" from="23492283#2" to="35549511#1"/>
    <trip id="veh1256" type="veh_passenger" depart="3281.90" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh1261" type="veh_passenger" depart="3294.96" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh1263" type="veh_passenger" depart="3300.19" departLane="best" from="-543400544#0" to="116315042"/>
    <trip id="veh1264" type="veh_passenger" depart="3302.80" departLane="best" from="775343565#0" to="87408929#4"/>
    <trip id="veh1265" type="veh_passenger" depart="3305.41" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh1266" type="veh_passenger" depart="3308.03" departLane="best" departSpeed="max" from="23492230#0" to="87408929#4"/>
    <trip id="veh1267" type="veh_passenger" depart="3310.64" departLane="best" departSpeed="max" from="23492230#0" to="-543754801"/>
    <trip id="veh1268" type="veh_passenger" depart="3313.25" departLane="best" departSpeed="max" from="449606867" to="54856713"/>
    <trip id="veh1269" type="veh_passenger" depart="3315.87" departLane="best" departSpeed="max" from="-775343568#1" to="68506246#1"/>
    <trip id="veh1270" type="veh_passenger" depart="3318.48" departLane="best" departSpeed="max" from="68506239#0" to="543400545"/>
    <trip id="veh1271" type="veh_passenger" depart="3321.09" departLane="best" departSpeed="max" from="-87408929#4" to="230423229"/>
    <trip id="veh1272" type="veh_passenger" depart="3323.70" departLane="best" from="23492230#2" to="68506246#1"/>
    <trip id="veh1273" type="veh_passenger" depart="3326.32" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh1274" type="veh_passenger" depart="3328.93" departLane="best" departSpeed="max" from="30014906" to="30014919"/>
    <trip id="veh1275" type="veh_passenger" depart="3331.54" departLane="best" departSpeed="max" from="23492230#0" to="116315042"/>
    <trip id="veh1276" type="veh_passenger" depart="3334.16" departLane="best" departSpeed="max" from="68506239#0" to="775343568#1"/>
    <trip id="veh1278" type="veh_passenger" depart="3339.38" departLane="best" departSpeed="max" from="-775343568#1" to="35549511#1"/>
    <trip id="veh1280" type="veh_passenger" depart="3344.61" departLane="best" departSpeed="max" from="123214371#0" to="314417325#2"/>
    <trip id="veh1281" type="veh_passenger" depart="3347.22" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh1282" type="veh_passenger" depart="3349.83" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh1283" type="veh_passenger" depart="3352.45" departLane="best" departSpeed="max" from="449606867" to="54856677#1"/>
    <trip id="veh1284" type="veh_passenger" depart="3355.06" departLane="best" departSpeed="max" from="-170958183#1" to="68506246#1"/>
    <trip id="veh1286" type="veh_passenger" depart="3360.29" departLane="best" departSpeed="max" from="449606871" to="68506264"/>
    <trip id="veh1287" type="veh_passenger" depart="3362.90" departLane="best" departSpeed="max" from="68506239#0" to="122307620#3"/>
    <trip id="veh1288" type="veh_passenger" depart="3365.51" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1289" type="veh_passenger" depart="3368.12" departLane="best" departSpeed="max" from="54856708#0" to="116315042"/>
    <trip id="veh1291" type="veh_passenger" depart="3373.35" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
    <trip id="veh1292" type="veh_passenger" depart="3375.96" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh1293" type="veh_passenger" depart="3378.58" departLane="best" departSpeed="max" from="-543400544#1" to="-122307619#0"/>
    <trip id="veh1294" type="veh_passenger" depart="3381.19" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh1295" type="veh_passenger" depart="3383.80" departLane="best" from="543400545" to="68506246#1"/>
    <trip id="veh1296" type="veh_passenger" depart="3386.42" departLane="best" departSpeed="max" from="54856708#0" to="32036700#4"/>
    <trip id="veh1297" type="veh_passenger" depart="3389.03" departLane="best" departSpeed="max" from="-543400544#1" to="23247531#9"/>
    <trip id="veh1299" type="veh_passenger" depart="3394.25" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh1300" type="veh_passenger" depart="3396.87" departLane="best" departSpeed="max" from="54856708#0" to="54856713"/>
    <trip id="veh1302" type="veh_passenger" depart="3402.09" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh1303" type="veh_passenger" depart="3404.71" departLane="best" departSpeed="max" from="-775343568#1" to="54856713"/>
    <trip id="veh1305" type="veh_passenger" depart="3409.93" departLane="best" departSpeed="max" from="-543400544#1" to="-123214371#0"/>
    <trip id="veh1306" type="veh_passenger" depart="3412.55" departLane="best" departSpeed="max" from="123214371#0" to="543400544#1"/>
    <trip id="veh1308" type="veh_passenger" depart="3417.77" departLane="best" departSpeed="max" from="449606871" to="54856677#1"/>
    <trip id="veh1309" type="veh_passenger" depart="3420.38" departLane="best" departSpeed="max" from="-775343568#1" to="26293951#1"/>
    <trip id="veh1310" type="veh_passenger" depart="3423.00" departLane="best" departSpeed="max" from="68506239#0" to="68506239#8"/>
    <trip id="veh1311" type="veh_passenger" depart="3425.61" departLane="best" departSpeed="max" from="449606867" to="54856677#1"/>
    <trip id="veh1314" type="veh_passenger" depart="3433.45" departLane="best" departSpeed="max" from="54856708#0" to="122307631#0"/>
    <trip id="veh1315" type="veh_passenger" depart="3436.06" departLane="best" departSpeed="max" from="68506239#0" to="543400544#1"/>
    <trip id="veh1316" type="veh_passenger" depart="3438.67" departLane="best" from="916493566#5" to="-122307616"/>
    <trip id="veh1317" type="veh_passenger" depart="3441.29" departLane="best" departSpeed="max" from="-775343568#1" to="87408929#4"/>
    <trip id="veh1319" type="veh_passenger" depart="3446.51" departLane="best" departSpeed="max" from="123214371#0" to="23247531#9"/>
    <trip id="veh1321" type="veh_passenger" depart="3451.74" departLane="best" departSpeed="max" from="449606871" to="54856713"/>
    <trip id="veh1322" type="veh_passenger" depart="3454.35" departLane="best" from="23247531#4" to="116315042"/>
    <trip id="veh1324" type="veh_passenger" depart="3459.58" departLane="best" departSpeed="max" from="54856708#0" to="262841306#2"/>
    <trip id="veh1325" type="veh_passenger" depart="3462.19" departLane="best" departSpeed="max" from="449606867" to="543400544#1"/>
    <trip id="veh1326" type="veh_passenger" depart="3464.80" departLane="best" departSpeed="max" from="-775343568#1" to="116315042"/>
    <trip id="veh1327" type="veh_passenger" depart="3467.42" departLane="best" departSpeed="max" from="-262841306#2" to="87408929#4"/>
    <trip id="veh1328" type="veh_passenger" depart="3470.03" departLane="best" departSpeed="max" from="449606867" to="68506246#1"/>
    <trip id="veh1329" type="veh_passenger" depart="3472.64" departLane="best" departSpeed="max" from="-775343568#1" to="54856677#1"/>
    <trip id="veh1332" type="veh_passenger" depart="3480.48" departLane="best" departSpeed="max" from="54856708#0" to="543400544#1"/>
    <trip id="veh1333" type="veh_passenger" depart="3483.10" departLane="best" departSpeed="max" from="449606867" to="54856677#1"/>
    <trip id="veh1334" type="veh_passenger" depart="3485.71" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh1335" type="veh_passenger" depart="3488.32" departLane="best" from="910000958" to="230423229"/>
    <trip id="veh1336" type="veh_passenger" depart="3490.93" departLane="best" departSpeed="max" from="-170958183#1" to="54856677#1"/>
    <trip id="veh1338" type="veh_passenger" depart="3496.16" departLane="best" departSpeed="max" from="449606871" to="129648702#1"/>
    <trip id="veh1339" type="veh_passenger" depart="3498.77" departLane="best" departSpeed="max" from="68506239#0" to="54856677#1"/>
    <trip id="veh1341" type="veh_passenger" depart="3504.00" departLane="best" departSpeed="max" from="54856708#0" to="68506246#1"/>
    <trip id="veh1342" type="veh_passenger" depart="3506.61" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1343" type="veh_passenger" depart="3509.23" departLane="best" departSpeed="max" from="-543400544#1" to="230423229"/>
    <trip id="veh1344" type="veh_passenger" depart="3511.84" departLane="best" departSpeed="max" from="54856708#0" to="54856708#1"/>
    <trip id="veh1345" type="veh_passenger" depart="3514.45" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh1346" type="veh_passenger" depart="3517.06" departLane="best" departSpeed="max" from="-87408929#4" to="116315042"/>
    <trip id="veh1348" type="veh_passenger" depart="3522.29" departLane="best" departSpeed="max" from="-87408929#4" to="230423229"/>
    <trip id="veh1349" type="veh_passenger" depart="3524.90" departLane="best" departSpeed="max" from="-775343568#1" to="-553278873#3"/>
    <trip id="veh1353" type="veh_passenger" depart="3535.36" departLane="best" departSpeed="max" from="123214371#0" to="23247531#9"/>
    <trip id="veh1354" type="veh_passenger" depart="3537.97" departLane="best" departSpeed="max" from="-262841306#2" to="68506239#11"/>
    <trip id="veh1355" type="veh_passenger" depart="3540.58" departLane="best" departSpeed="max" from="-543400544#1" to="116315042"/>
    <trip id="veh1356" type="veh_passenger" depart="3543.19" departLane="best" departSpeed="max" from="23492230#0" to="543400544#1"/>
    <trip id="veh1358" type="veh_passenger" depart="3548.42" departLane="best" departSpeed="max" from="-87408929#4" to="54856677#1"/>
    <trip id="veh1359" type="veh_passenger" depart="3551.03" departLane="best" departSpeed="max" from="449606867" to="-116801392#3"/>
    <trip id="veh1362" type="veh_passenger" depart="3558.87" departLane="best" departSpeed="max" from="68506239#0" to="262841306#2"/>
    <trip id="veh1363" type="veh_passenger" depart="3561.48" departLane="best" departSpeed="max" from="123214371#0" to="-122307619#1"/>
    <trip id="veh1364" type="veh_passenger" depart="3564.10" departLane="best" departSpeed="max" from="-87408929#4" to="54856713"/>
    <trip id="veh1365" type="veh_passenger" depart="3566.71" departLane="best" departSpeed="max" from="-543400544#1" to="54856677#1"/>
    <trip id="veh1366" type="veh_passenger" depart="3569.32" departLane="best" departSpeed="max" from="449606871" to="23247531#9"/>
    <trip id="veh1368" type="veh_passenger" depart="3574.55" departLane="best" departSpeed="max" from="54856708#0" to="23247531#9"/>
    <trip id="veh1369" type="veh_passenger" depart="3577.16" departLane="best" from="830793057#1" to="23247531#9"/>
    <trip id="veh1370" type="veh_passenger" depart="3579.78" departLane="best" departSpeed="max" from="68506239#0" to="-830793056#0"/>
    <trip id="veh1371" type="veh_passenger" depart="3582.39" departLane="best" departSpeed="max" from="-87408929#4" to="68506246#1"/>
    <trip id="veh1372" type="veh_passenger" depart="3585.00" departLane="best" departSpeed="max" from="68506239#0" to="68506246#1"/>
    <trip id="veh1374" type="veh_passenger" depart="3590.23" departLane="best" departSpeed="max" from="449606871" to="543400544#1"/>
    <trip id="veh1375" type="veh_passenger" depart="3592.84" departLane="best" departSpeed="max" from="449606867" to="-123214371#0"/>
    <trip id="veh1376" type="veh_passenger" depart="3595.45" departLane="best" departSpeed="max" from="54856708#0" to="230423229"/>
"""

# Find all trip entries and extract the 'from' and 'to' values
trip_pattern = re.compile(r'<trip id="veh(\d+)" type="veh_passenger" depart="\d+\.\d+" departLane="best"( departSpeed="max")? from="([^"]+)" to="([^"]+)"/>')
matches = trip_pattern.findall(input_data)

# Create the route entries
routes = [f'<route id="{match[0]}" edges="{match[2]} {match[3]}"/>' for match in matches]

# Join all the route entries into a single string
output_data = '\n'.join(routes)

print(output_data)
