import traci
import utm
import time
import socket
import threading
import json
import random
import argparse

class Server:
    """
    Server from which each client will connect to. and get the positions of the vehicles.
    """
    def __init__(self, address : str = "0.0.0.0", port : int = 9000) -> None:
        self.stop_event = threading.Event()
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.bind((address, port))
        self.socket.listen()
        self.clients = []
        self.new_client_thread = threading.Thread(target=self.accept_clients)
        self.new_client_thread.start()
    
    def accept_clients(self):
        while not self.stop_event.is_set():
            client, _ = self.socket.accept()
            self.clients.append(client)
    
    def send_positions(self, position : str):
        to_send = position.encode("utf-8")
        clients_to_remove = []
        for client in self.clients:
            try: 
                client.send(to_send)
            except:
                clients_to_remove.append(client)
        for client in clients_to_remove:
            self.clients.remove(client)

class SUMOHandler:
    """
    This class handles the sumo simulation. Retreives the position from it so later
    it can be sent to the clients.
    """
    offset_x : float
    offset_y : float
    zone_letter : str
    zone_number : int
    simulation_delay : float
    server : Server
    ending_event : threading.Event
    
    def __init__(self, offset_x: float, offset_y: float, simulation_delay: float,
                server: Server, sumo_binary: str = "/usr/bin/sumo-gui",
                sumo_config: str = "sumo_config.sumocfg",
                zone_number: int = 31, zone_letter: str = "T"):
        self.offset_x = offset_x
        self.offset_y = offset_y
        self.zone_number = zone_number
        self.zone_letter = zone_letter
        self.simulation_delay = simulation_delay
        self.vehicle_id_counter = 0
        self.server = server
        self.ending_event = threading.Event()
        sumo_cmd = [sumo_binary, "-c", sumo_config]
        self.simulation_thread = threading.Thread(target=self.start_simulation, args=(sumo_cmd,))
        self.simulation_thread.start()
        
    def start_simulation(self, sumo_cmd: list[str]):
        timestamp_in_millis = time.time()*1000
        traci.start(sumo_cmd)
        while not self.ending_event.is_set():
            self.add_vehicles()
            start_time = time.time()
            for veh_id in traci.vehicle.getIDList():
                x, y = traci.vehicle.getPosition(veh_id)
                heading = traci.vehicle.getAngle(veh_id)
                speed = traci.vehicle.getSpeed(veh_id)
                acceleration = traci.vehicle.getAcceleration(veh_id)
                x += self.offset_x
                y += self.offset_y
                lat, lon = utm.to_latlon(x, y, self.zone_number, self.zone_letter)
                to_send = {
                    "timestamp": timestamp_in_millis,
                    "vehicle_id": veh_id,
                    "latitude": lat,
                    "longitude": lon,
                    "speed": speed,
                    "heading": heading,
                    "acceleration": acceleration
                }
                to_send = json.dumps(to_send)
                self.server.send_positions(to_send)
            duration = time.time() - start_time
            traci.simulationStep()
            time.sleep(self.simulation_delay-duration)
            timestamp_in_millis += self.simulation_delay*1000
            

    def add_vehicles(self) -> None:
        """
        Add the vehicles to the simulation.
        
        it tries to keep a certain number of vehicles on the simulation.
        """
        vehicles = traci.vehicle.getIDList()
        num_of_vehicles = len(vehicles)
        while num_of_vehicles < 40:
            self.vehicle_id_counter += 1
            routes = traci.route.getIDList()
            new_route = routes[int(random.uniform(0, len(routes)))]
            traci.vehicle.add(str(self.vehicle_id_counter), routeID=new_route, departLane='best', departPos=0)    # typeID=car_type,
            num_of_vehicles += 1



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Mock Freshness Simulator")
    parser.add_argument("--server-address", type=str, default="0.0.0.0", help="Server address")
    parser.add_argument("--server-port", type=int, default=9000, help="Server port")
    parser.add_argument("--offset-x", type=float, default=0, help="Offset in x direction")
    parser.add_argument("--offset-y", type=float, default=0, help="Offset in y direction")
    parser.add_argument("--simulation-delay", type=float, default=1, help="Simulation delay")
    parser.add_argument("--sumo-binary", type=str, default="/usr/bin/sumo-gui", help="SUMO binary path")
    parser.add_argument("--sumo-config", type=str, default="sumo_config.sumocfg", help="SUMO configuration file")
    parser.add_argument("--zone-number", type=int, default=31, help="Zone number")
    parser.add_argument("--zone-letter", type=str, default="T", help="Zone letter")

    args = parser.parse_args()
    
    server = Server(address=args.server_address, port=args.server_port)
    sumo_handler = SUMOHandler(offset_x=args.offset_x, offset_y=args.offset_y, simulation_delay=args.simulation_delay,
                            server=server, sumo_binary=args.sumo_binary, sumo_config=args.sumo_config,
                            zone_number=args.zone_number, zone_letter=args.zone_letter)
    try:
        server.new_client_thread.join()
        sumo_handler.simulation_thread.join()
    except KeyboardInterrupt:
        sumo_handler.ending_event.set()
        server.stop_event.set()
        sumo_handler.simulation_thread.join()
        server.new_client_thread.join() 
        traci.close()
