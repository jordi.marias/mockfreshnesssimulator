FROM ubuntu:jammy

RUN apt-get update &&\
    apt-get install -y software-properties-common &&\
    add-apt-repository ppa:sumo/stable &&\ 
    apt-get update &&\
    apt-get install -y sumo sumo-tools sumo-doc

RUN apt-get install -y python3 python3-pip


ADD requirements.txt /requirements.txt
RUN pip3 install -r /requirements.txt
ADD src/server.py /server.py
ADD scenario/ /scenario/

# Command to run on container start
ENTRYPOINT [ "python3" , "-u", "/server.py"]
CMD ["--server-address", "0.0.0.0", "--server-port", "9000", "--offset-x", "424867.86", "--offset-y", "4580733.51", "--simulation-delay", "1", "--sumo-binary", "/usr/bin/sumo", "--sumo-config", "/scenario/osm.sumocfg", "--zone-number", "31", "--zone-letter", "T"]
